*****************************************************
Armv-A Base RevC AEM FVP Platform Software User Guide
*****************************************************

.. contents::

Introduction
############

The Armv-A Base RevC AEM FVP (Fixed Virtual Platform) is an evolution of the
base platform, enhanced to support the exploration of system level
virtualization.
It is an Architecture Envelope Model (AEM) which incorporates two AEM
clusters, each of which can be configured with up to four cores.

This document is a user guide on how to set up, build and run the
software stack on the Armv-A Base RevC AEM FVP (Fixed Virtual Platform).

This software stack includes a Dockerfile which creates a guest container for
downloading, building and running the software stack.
This guide provides instructions to use the container and to debug common
issues that likely to be encountered whilst developing within the container
environment.


Validated build environments
############################

The build-system has been validated in the included `Docker`_ container and
directly in Ubuntu 20.04 LTS but **only the Docker container is fully
supported**.

Docker container
================

The build environment for the AEMFVP-A software stack is now provisioned by an
Ubuntu 20.04 based Docker container that once built can be used to build all
the software components as mentioned in the following section.

To install Docker on the system, follow the `Standard Installation of Docker`_
page.

**Note:** The Docker implementation has been tested on an Ubuntu 20.04 host
system.

Prerequisites:

- Docker installed
- Minimum of 50 GB free storage space.
- Commands provided in this guide are executed from a ``bash`` shell
  environment.


Docker preparation
******************

Follow the commands below to fetch the ``Docker`` repository for AEMFVP-A,
build the Docker container and make it ready for the user to execute build
commands.

For more details on the Docker environment refer to the `AEMFVP-A Docker
Readme`_.

- Fetch and build the Docker container

  ::

    git clone https://git.gitlab.arm.com/arm-reference-solutions/docker.git -b AEMFVP-A-2023.09.08
    docker build -t arm-reference-builder docker/

- Create a workspace directory

  ::

    mkdir <aemfvp-a_workspace>
    cd <aemfvp-a_workspace>

- Create an alias for the ``docker run`` command as ``docker_run`` for re-usability

  ::

    alias docker_run="docker run --rm \
                      --mount type=bind,source=<absolute_path_to_aemfvp-a_workspace>,target=/workspace \
                      -e TERM=$TERM \
                      --user $(id -u):$(id -g) \
                      -it arm-reference-builder"

The ``docker_run`` alias can now be appended at the start of each build (or
repo) commands mentioned in this guide and all the builds will execute in the
built Docker container environment.

The bind mounted ``<aemfvp-a_workspace>`` helps in binding the AEMFVP-A
workspace between the host system and Docker environment.
This helps in flexibility of developing in the host and build in the Docker
environment where all the build dependencies are taken care of.

**NOTE:** If facing permission issues with docker, make sure to follow the
`post-installation steps`_.

Fetch the Software Stack
########################

For fetching the latest *stable* software stack, use the following commands

  ::

    docker_run repo init \
          -u https://git.gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-manifest.git \
          -m pinned-aemfvp-a.xml \
          -b refs/tags/AEMFVP-A-2023.09.08 \
          --depth=1
    docker_run repo sync

**Note:** ``--depth=1`` option is used with ``repo init`` command to have a
shallow clone of the projects.
This helps in fastening the fetch but results in the limited commit history of
the projects.
To have a fetch with complete commit history drop this option.
This might take a longer time in syncing the repo.


Obtaining the Armv-A Base RevC AEM FVP
######################################

The  recent release ``AEMFVP-A-2023.09.08`` has been tested and validated
with the ``Armv-A Base RevC AEM FVP`` releases listed below and are therefore
the recommended versions to use. However, check the `Arm Ecosystem FVPs`_ page
for newer releases and if a version not listed in the table is used, make sure
to check the FVP release notes as compatibility is not guaranteed.

+----------------------------------+----------------------------------------------------------------------------------------------------------------------+
| Armv-A Base RevC AEM FVP Version | URL                                                                                                                  |
+==================================+======================================================================================================================+
| 11.22_14 (x86 Linux)             | https://developer.arm.com/-/media/Files/downloads/ecosystem-models/FVP_Base_RevC-2xAEMvA_11.22_14_Linux64.tgz        |
+----------------------------------+----------------------------------------------------------------------------------------------------------------------+
| 11.22_14 (Aarch64 Linux)         | https://developer.arm.com/-/media/Files/downloads/ecosystem-models/FVP_Base_RevC-2xAEMvA_11.22_14_Linux64_armv8l.tgz |
+----------------------------------+----------------------------------------------------------------------------------------------------------------------+

The tar file can be extracted to any directory of choice.
After extraction the model executable can be found as

- ``Base_RevC_AEMvA_pkg/models/Linux64_GCC-9.3/FVP_Base_RevC-2xAEMvA``
  for the x86-Linux
- ``Base_RevC_AEMvA_pkg/models/Linux64_armv8l_GCC-9.3/FVP_Base_RevC-2xAEMvA``
  for the Aarch64-Linux

FVP specific documentation can be found under ``Base_RevC_AEMvA_pkg/doc``.

To ensure that all the required packages for FVP are installed, run:

::

    sudo apt-get install telnet xterm


Enable network on Armv-A Base RevC AEM FVP
==========================================

The Armv-A Base RevC AEM FVP supports virtual ethernet interface to allow
networking support, used for the software executed by the Armv-A Base RevC AEM
FVP.
If support for networking is required, the host TAP interface has to be set up
before the Armv-A Base RevC AEM FVP is launched.
To set up the TAP interface, execute the following commands on the host
machine.

- Install libvirt

  ::

    sudo apt-get install libvirt-daemon-system

  Note: The above command creates the network interface ``virbr0`` with the
  IP address ``192.168.122.1``. This can be checked with the command ``ifconfig``.
  If the interface is not created, run the following command to restart the
  libvirt daemon.

  ::

    sudo systemctl restart libvirtd

- Create a tap interface named 'tap0'

  ::

    sudo ip tuntap add dev tap0 mode tap user $(whoami)
    sudo ifconfig tap0 0.0.0.0 promisc up
    sudo brctl addif virbr0 tap0

Supported features
##################

Armv-A Base RevC AEM FVP software stack supports booting the following features.

- `BusyBox`_
- `Linux distribution`_

The following sections explain the build and boot steps for of both the
features.

BusyBox
=======

Build the platform software
***************************

This section describes the procedure to build the following software components
for BusyBox boot.

        - Trusted firmware (TF-A)
        - U-Boot
        - UEFI
        - GRUB
        - Linux
        - BusyBox

The disk image consists of two partitions.
The first partition is an EFI system partition and contains GRUB and the kernel
Image.
The second partition is an ext4 partition which contains the BusyBox based
rootfs.
Examples of how to use the build command for BusyBox boot are listed below.

To build the software stack, the command to be used is

::

  docker_run ./build-scripts/aemfvp-a/build-test-busybox.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - ``aemfvp-a``

* <command>

  - Supported commands are

    - ``clean``
    - ``build``
    - ``package``
    - ``all`` (all of the three above)

**Note:** On networks where the git port is blocked, the build procedure might not
progress. Refer to the `troubleshooting guide`_ for possible ways to resolve
this issue.

Examples of the build command are

- Command to clean, build and package the software stack required for BusyBox
  boot on Armv-A Base RevC AEM FVP platform:

  ::

    docker_run ./build-scripts/aemfvp-a/build-test-busybox.sh -p aemfvp-a all

- Command to perform an incremental build of the software components included in
  the software stack for the Armv-A Base platform.

  Note: this command should be followed by the ``package`` command to complete
  the preparation of the FIP and the disk image.

  ::

    docker_run ./build-scripts/aemfvp-a/build-test-busybox.sh -p aemfvp-a build

- Command to package the previously built software stack and prepare the FIP and
  the disk image.

  ::

    docker_run ./build-scripts/aemfvp-a/build-test-busybox.sh -p aemfvp-a package

The user can also rebuild the individual software components if needed. Each
software component has a separate script to build that component. Once built,
the components must be packaged as a disk image.

Building the Software components (individually)
***********************************************

Each component has a separate script to build the respective component. The
following table lists the commands to build/clean a component binary along
with the respective output paths for the generated binaries.

**Note:** The following table shows the commands for ``busybox`` (or ``none``)
filesystem, as there is no ``distribution`` usage for filesystem flag, also
there is no separate script to create the installable distribution images.

.. list-table::
   :widths: 20 150 150
   :header-rows: 1

   * - Software Component
     - Build/Clean Command
     - Output
   * - `Trusted Firmware-A`_ (TF-A)
     - docker_run ./build-scripts/build-arm-tf.sh -p aemfvp-a -f <busybox/none> <build/clean>
     - <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/tf-bl1.bin ,
       <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/tf-bl2.bin ,
       <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/tf-b31.bin
   * - `U-Boot`_
     - docker_run ./build-scripts/build-uboot.sh -p aemfvp-a -f <busybox/none> <build/clean>
     - <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/uboot.bin
   * - UEFI
       (`edk2`_ and `edk2-platforms`_)
     - docker_run ./build-scripts/build-uefi.sh -p aemfvp-a -f <busybox/none> <build/clean>
     - <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/uefi.bin
   * - `Grub`_
     - docker_run ./build-scripts/build-grub.sh -p aemfvp-a -f <busybox/none> <build/clean>
     - <aemfvp-a_workspace>/grub/output/grubaa64.efi
   * - `Linux`_
     - docker_run ./build-scripts/build-linux.sh -p aemfvp-a -f busybox <build/clean>
     - <aemfvp-a_workspace>/output/aemfvp-a/aemfvp-a/Image
   * - `BusyBox`_
     - docker_run ./build-scripts/build-busybox.sh -p aemfvp-a -f busybox <build/clean>
     - <aemfvp-a_workspace>/busybox/out/arm64/_install/


Once the components are built, they need to be packaged to the BusyBox disk
image using the following command.
(no packaging required in case of Distribution boot)

::

    docker_run ./build-scripts/aemfvp-a/build-test-busybox.sh -p aemfvp-a package


Booting BusyBox
***************

After the build of the platform software stack for BusyBox is complete, the
following commands can be used to start the execution of the Armv-A Base RevC AEM FVP
model and boot the platform up to the BusyBox prompt. Examples of how to use the
commands are listed below.

To boot up to the BusyBox prompt, the commands to be used are

- Set ``MODEL`` path before launching the model (`Obtaining the Armv-A Base RevC AEM FVP`_):

  ::

    export MODEL=<absolute path to the platform Armv-A AEM FVP binary>

- Launch BusyBox boot:

  ::

    ./model-scripts/aemfvp-a/boot.sh -p <platform name> -b <bootloader> -n [true|false]


Supported command line options are listed below

*  -p <platform name>

   - ``aemfvp-a``

*  -b <bootloader>

   - Select bootloader to boot with BusyBox.

   - ``uefi`` (Default)
   - ``u-boot``

*  -n [true|false] (optional)

   - Enable or disable network controller support on the platform.
     If network ports have to be enabled, use 'true' as the option.
     Default value is set to 'false'.

Example commands for booting BusyBox are listed below.

- Command to start the execution of the Armv-A Base model to boot up to the
  BusyBox prompt using UEFI:

  ::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a -b uefi

- Command to start the execution of the Armv-A Base model to boot up to the
  BusyBox prompt using U-Boot:

  ::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a -b u-boot

- Command to start the execution of the Armv-A Base model to boot up to the
  BusyBox prompt. The selection of the bootloader to be either UEFI or U-Boot
  is based on command line parameters. The model supports networking allowing
  the software running within the model to access the network.

  ::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a -b [uefi|u-boot] -n true

  Note: If the user stops autoboot while booting with U-Boot and enters the
  U-Boot command line console, the user must run the following command to
  continue the boot process.

  ::

    run bootcmd

  Command to exit from the command line console.

  ::

     Ctrl + ]
     telnet> close

When the script is executed, four terminal instances will be launched. The
usage of each terminal can be seen below,

        - Terminal-0 is the debug console for the AP (Application Processor) and
          contains the booting logs of Trusted firmware-A, U-Boot, Linux, and
          user-space applications.
        - Terminal-1 is the debug console for the AP (Application Processor) and
          contains the UEFI boot logs.
        - Terminal-2 is the debug console that displays the localhost information.
        - The fourth Terminal uses the GUI representation of the model, which
          contains information about the overall executed instructions of the
          CPUs and the status of each CPU in the clusters.

The AP will start booting Trusted Firmware-A, followed by UEFI/U-Boot, Linux,
and then BusyBox.

To run terminal-only mode on hosts without graphics/display:

::

        env -u DISPLAY ./model-scripts/aemfvp-a/boot.sh -p <platform name> -b <bootloader>


This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close


Note: The boot logs can be found at ``<aemfvp-a_workspace>/aemfvp-a`` path after
booting BusyBox. The following logs are generated in this path.

        - uart0.log: Terminal-0 debug console logs are stored in the uart0.log
          file. It is a symbolic link for uart0 log file with the latest timestamp.
        - uart1.log: Terminal-1 debug console logs are stored in the uart1.log
          file. It is a symbolic link for uart1 log file with the latest timestamp.

Linux distribution
==================

The Armv-A Base RevC AEM FVP platform software stack supports the installation
and boot of various Linux distributions such as Debian, Ubuntu, and Fedora. The
distribution is installed on a SATA disk and since the installed image is
persistent it can be used for multiple boots.

Build the platform software
***************************

This section describes the procedure to build the platform firmware required to
install and boot a Linux distribution on Armv-A Base RevC AEM FVP platforms.

To build the Armv-A Base RevC AEM FVP software stack, the command to be used is

::

  docker_run ./build-scripts/build-test-uefi.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - ``aemfvp-a``

* <command>

  - ``clean``
  - ``build``
  - ``package``
  - ``all`` (all of the three above)


Examples of the build command are

- Command to clean, build and package the Armv8-A Base AEM FVP software stack required for
  the distribution installation/boot on the platform:

  ::

    docker_run ./build-scripts/build-test-uefi.sh -p aemfvp-a all

- Command to remove the generated outputs (binaries) of the software stack for
  the Armv8-A Base FVP platform:

  ::

    docker_run ./build-scripts/build-test-uefi.sh -p aemfvp-a clean

- Command to perform an incremental build of the software components included in
  the software stack for the Armv8-A Base platform:

  Note: this command should be followed by the ``package`` command to
  complete the preparation of the FIP image.

  ::

    docker_run ./build-scripts/build-test-uefi.sh -p aemfvp-a build

- Command to package the previously built software stack and prepares the FIP
  image:

  ::

    docker_run ./build-scripts/build-test-uefi.sh -p aemfvp-a package


Installing a Linux distribution
*******************************

After the platform firmware build for the distribution install/boot is complete,
a distribution can be installed into a SATA disk image. Before beginning the
installation process, download the CD ISO image of the required distribution
version.

The distribution installation images can be downloaded from the
following locations (select an image built for aarch64 architecture):

- `Fedora-36-1.5`_
- `Ubuntu-20.04.4`_
- `Debian-12.1.0`_

**Important:**
Although installing and booting of Linux distribution are supported, it
should be noted that the FVP model is observed to be a little slow with this.
Debian-12.1.0 installer has been used to validate this where it took around
30 minutes for the installer to start and 7 hours to finish the installation.
This time is only for reference and the actual time might be less or more for
each user, depending on the choice of the distribution, and the user's host
system.

The commands used to begin the distribution installation are:

- Set ``MODEL`` path before launching the model (`Obtaining the Armv-A Base RevC AEM FVP`_):

  ::

    export MODEL=<absolute path to the platform FVP binary>

- Launch the installation:

  ::

    ./model-scripts/aemfvp-a/distro.sh -p <platform name> -i <abs_iso_image_path> -s <disk size> -n [true|false]

Supported command line options are listed below

*  -p <platform name>

   - ``aemfvp-a``

*  -i <abs_iso_image_path>

   - Absolute path to the downloaded distribution installer disk image.

*  -s <disk_size>

   - Size of the SATA disk image (in GB) to be created. 12GB and above is
     good enough for most use cases.

*  -n [true|false] (optional)

   - Enable or disable network controller support on the platform.
     If network ports have to be enabled, use 'true' as the option.
     Default value is set to 'false'.


An example of a command to install the Debian distribution is listed below.

::

  ./model-scripts/aemfvp-a/distro.sh -p aemfvp-a -i <absolute-path-to>/debian-12.1.0-arm64-netinst.iso -s 16 -n true

- This command creates a 16GB SATA disk image, boots the Armv8-A Base
  software stack and starts the installation of Debian distribution.

- From here on, follow the instructions of the chosen distribution
  installer. For more information about the installation procedure, refer
  online installation manuals of the chosen distribution.

- After the installation is completed, the disk image with a random name
  "<number>.satadisk" will be created in satadisk/ folder.
  Use this disk image for booting the installed distribution.


Additional distribution specific instructions
---------------------------------------------

- **Debian**

  - It is recommended to install Debian using the "Expert Install" mode present
    in the "Advanced Options" from the Debian installation menu and during the
    "Install the GRUB boot loader" stage, select <YES> when asked to 'force GRUB
    installation to the EFI removable media path".

  - Sometimes with the standard "Install" it is observed that debian is not able
    to boot from the corresponding satadisk drive. In such a case, press 'Esc'
    on the AP terminal during the boot to enter the UEFI menu. From the menu enter
    the UEFI shell and execute "grubaa64.efi" from "<FS2:>/EFI/debian".

Booting a Linux distribution
****************************

To boot the installed distribution, use the following commands:

- Set ``MODEL`` path before launching the model (`Obtaining the Armv-A Base RevC AEM FVP`_):

  ::

    export MODEL=<absolute path to the platform FVP binary>

- Start the distribution boot:

  ::

    ./model-scripts/aemfvp-a/distro.sh -p <platform name> -d <satadisk_path> -n [true|false]

Supported command line options are listed below

*  -p <platform name>

   - ``aemfvp-a``

*  -d <satadisk_path>

   -  Absolute path to the installed distribution disk image created using the
      instructions listed in the previous section.

*  -n [true|false] (optional)

   - Enable or disable network controller support on the platform.
     If network ports have to be enabled, use 'true' as the option.
     Default value is set to 'false'.

Example commands to boot a Linux distribution are listed below.

- Command to look for the available ``.satadisk`` image in the
  ``satadisk/`` folder and boots with that image. If multiple
  ``.satadisk`` images are found, it will list them all but won't boot:

  ::

    ./model-scripts/aemfvp-a/distro.sh -p aemfvp-a


- Command to begin the distribution boot from the ``<debian>.satadisk`` image:

  ::

    ./model-scripts/aemfvp-a/distro.sh -p aemfvp-a -d satadisk/<debian>.satadisk

When the script is executed, four terminal instances will be launched. The
usage of each terminal can be seen below,

        - Terminal-0 is the debug console for the AP (Application Processor) and
          contains the booting logs of Trusted firmware-A, Linux, and user-space
          applications.
        - Terminal-1 is the debug console for the AP (Application Processor) and
          contains the UEFI boot logs.
        - Terminal-2 is the debug console that displays the localhost information.
        - The fourth Terminal uses the GUI representation of the model, which
          contains information about the overall executed instructions of the
          CPUs and the status of each CPU in the clusters.

The AP will start booting Trusted Firmware-A, followed by UEFI, Linux, and then
Distribution.

To run terminal-only mode on hosts without graphics/display:

::

        env -u DISPLAY ./model-scripts/aemfvp-a/distro.sh -p <platform name> -d <satadisk_path>


This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close


Note: The boot logs can be found at ``<aemfvp-a_workspace>/aemfvp-a`` path after
booting distribution. The following logs are generated in this path.

        - uart0.log: Terminal-0 debug console logs are stored in the uart0.log
          file. It is a symbolic link for uart0 log file with the latest timestamp.
        - uart1.log: Terminal-1 debug console logs are stored in the uart1.log
          file. It is a symbolic link for uart1 log file with the latest timestamp.

Using cloud images to boot a distribution
-----------------------------------------

As an alternative to the standard process of installing a distribution using the
standard installers, the cloud images can be used. This allows to directly boot
a raw installed image of the distribution without having to perform the
installation procedures.

Debian Distribution
^^^^^^^^^^^^^^^^^^^

This section decribes using the cloud image to boot ``Debian`` distribution.

The cloud images for Debian can be obtained from the `Debian cloud images`_ page.
A number of images from the previous releases, along with the daily builds of the
latest release version are available on this page. The following cloud image has
been used to validate the Debian distribution boot.

- Debian 12 Bookworm cloud image : https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.raw

**Note:** It is always recommended to use the ``nocloud`` variant of the Debian
cloud image as it provisions the user to login as root without a password.

Once the image is downloaded, it can be directly used to boot the Debian distribution
as described below:

::

        ./model-scripts/aemfvp-a/distro.sh -p aemfvp-a -d <path_to_downloaded_cloud_image> -n [true|false]

Here,

-  ``-n [true|false]`` (optional) can be used to enable or disable network controller
   support on the platform. Default value is set to 'false' which indicates that the
   network ports are disabled.

To run terminal-only mode on hosts without graphics/display:

::

        env -u DISPLAY ./model-scripts/aemfvp-a/distro.sh -p aemfvp-a -d <path_to_downloaded_cloud_image> -n [true|flase]


This launches FVP in the background and automatically connects to the interactive
application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close

Report security vulnerabilities
###############################

For reporting security vulnerabilities, please refer `Vulnerability reporting`_
page.


--------------

*Copyright (c) 2021-2023, Arm Limited. All rights reserved.*

.. _AEMFVP-A Docker Readme: https://gitlab.arm.com/arm-reference-solutions/docker/-/blob/aemfvp-a/README.md
.. _Arm Ecosystem FVPs: https://developer.arm.com/downloads/-/arm-ecosystem-models
.. _Debian-12.1.0: https://cdimage.debian.org/debian-cd/current/arm64/iso-cd/debian-12.1.0-arm64-netinst.iso
.. _Docker: https://gitlab.arm.com/arm-reference-solutions/docker/-/tree/aemfvp-a
.. _Fedora-36-1.5: https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/36/Server/aarch64/iso/Fedora-Server-dvd-aarch64-36-1.5.iso
.. _Grub: https://www.gnu.org/software/grub/grub-download.html
.. _Linux: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
.. _Standard Installation of Docker: https://docs.docker.com/engine/install/
.. _Troubleshooting guide: docs/aemfvp-a/troubleshooting-guide.rst
.. _Trusted Firmware-A: https://git.trustedfirmware.org/TF-A/trusted-firmware-a
.. _U-Boot: https://git.denx.de/u-boot
.. _Ubuntu-20.04.4: https://old-releases.ubuntu.com/releases/focal/ubuntu-20.04.4-live-server-arm64.iso
.. _Vulnerability reporting: docs/aemfvp-a/vulnerability-reporting.rst
.. _edk2-platforms: https://github.com/tianocore/edk2-platforms
.. _edk2: https://github.com/tianocore/edk2
.. _Debian cloud images: https://cloud.debian.org/images/cloud/
.. _post-installation steps: https://docs.docker.com/engine/install/linux-postinstall

