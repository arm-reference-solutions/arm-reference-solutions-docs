*********************
Troubleshooting Guide
*********************

Builds do not progress to completion
====================================

During the build of the platform software stack, components such as GRUB
download additional code from remote repositories using the git port (or the
git protocol). Development machines on which git port is blocked, the build
does not progress to completion, waiting for the additional code to be
downloaded. This typically is observed when setting up a new platform software
workspace.

As a workaround, use https instead of git protocol for cloning required git
submodules of the various components in the software stack. A patch, as an
example of this change in the GRUB component, is listed below.

::

  diff --git a/bootstrap b/bootstrap
  index 5b08e7e2d..031784582 100755
  --- a/bootstrap
  +++ b/bootstrap
  @@ -47,7 +47,7 @@ PERL="${PERL-perl}"

   me=$0

  -default_gnulib_url=git://git.sv.gnu.org/gnulib
  +default_gnulib_url=https://git.savannah.gnu.org/git/gnulib.git

   usage() {
     cat <<EOF


--------------

*Copyright (c) 2021-2022, Arm Limited. All rights reserved.*

