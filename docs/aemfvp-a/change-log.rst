**********
Change Log
**********

This document contains a summary of the incremental changes, features, fixes
and known issues in each release of the Armv-A Base AEM FVP platform stack.

Release tag: AEMFVP-A-2023.09.08
================================
Changed
-------
- Updated Trusted Firmware-A to ``v2.9``
- Updated Linux to ``v6.3.8``
- Updated edk2 to ``edk2-stable202305``
- Updated edk2-platforms to ``748929b8439662319849ab73a1e554b114b17ffc``

Features
--------

- Support to boot Debian distribution using the cloud images.
- Support to execute the model on an AARCH64 host machine with the released
  AARCH64 version of the AEM FVP Model.

Platform Support
----------------

- This software release is tested on Armv-A Base RevC AEM FVP and version is
  [11.22_14 (Jun 14 2023)].
- The included Docker (v20.10.21) container has been tested on an Ubuntu 20.04
  system (x86-64 and Arm64).

Known issues or Limitations
---------------------------

- The Armv-A Base AEM FVP model hangs when Linux reboot is triggered after the
  model is booted with U-Boot and Busybox.
- The Virtio storage PCI modelling is broken with the AEM FVP Model 11.22_14
  and must be disabled with model parameters
  "``-C pci.pcidevice0.is_hidden=1 -C pci.pcidevice1.is_hidden=1``".
  This has already been done in the model scripts included with this software
  stack.

Release tag: AEMFVP-A-2022.12.09
================================
Changed
-------
- Updated Trusted Firmware-A to ``v2.8``
- Updated BusyBox to ``1.35``
- Updated Linux to ``v6.0.7``
- Updated U-Boot to ``v2021.10``
- Updated edk2 to ``edk2-stable202208``
- Updated edk2-platforms to ``0286233f3bfa5e0995256636e23d5efe779ab5e7``

Features
--------

- Support for building the software stack on x86-64 (cross-compile) and Arm64
  (native).
- Docker container to be used as the build environment.

Platform Support
----------------

- This software release is tested on Armv-A Base RevC AEM FVP and version is
  [11.20_15 (Dec 01 2022)].
- The included Docker (v20.10.12) container has been tested on an Ubuntu 20.04
  system (x86-64 and Arm64).

Known issues or Limitations
---------------------------

- The Armv-A Base AEM FVP model hangs when Linux reboot is triggered after the
  model is booted with U-Boot and Busybox.
- The Virtio storage PCI modelling is broken with the AEM FVP Model 11.20_15
  and must be disabled with model parameters
  "``-C pci.pcidevice0.is_hidden=1 -C pci.pcidevice1.is_hidden=1``".
  This has already been done in the model scripts included with this software
  stack.

Release tag: AEMFVP-A-2021.09.20
================================
Changed
-------
- Migration of build system from Yocto to BASH based scripting environment.
- Migration of source code repository from `ARM linaro GIT`_ to `ARM Gitlab`_.
- Migration of Kernel from 5.4 to 5.13.
- Update Trusted Firmware-A to version 2.5.
- Update U-boot to version 2021.4.
- Migrate to the latest stable EDK2 version ``edk2-stable202108``.
- Migrate to the latest EDK2-platforms commit ID ``46026ad759b718142e652618c399a1f68d4e3804``.
- Changed the Armv-A Base AEM FVP model parameters to enable all the CPU cores.
- Updated the Armv-A Base AEM FVP model parameters to enable network support.

Features
--------
- (AARCH64) UEFI + Busybox boot supported with latest stable kernel version 5.13.
- (AARCH64) U-Boot + Busybox boot supported with latest stable kernel version 5.13.
- Linux distribution boot supported and the following distributions are verified
  with Armv-A Base AEM FVP.

        - Fedora-34-1.2
        - Ubuntu-20.04.1
        - Debian-11.0.0

- Add changes for accessing Terminal-0 via Telnet if the Armv-A base AEM FVP
  is running on a non-Xserver support.

Platform Support
----------------

- This Software release is tested on Armv-A Base RevC AEM FVP and version is
  [11.15.18 (Jul 14 2021)].

Known issues or Limitations
---------------------------
- Ubuntu 20.04.1 installation may take longer time compared to other distribution
  installations on Armv-A Base AEM FVP.
- The Armv-A Base AEM FVP model hangs when Linux reboot is triggered after the
  model is booted with U-Boot and Busybox.

Change logs for the previous releases
=====================================
- Refer to the `Old Change Log`_ for detailed information on software features
  and changes for the previous releases.


--------------

*Copyright (c) 2021-2023, Arm Limited. All rights reserved.*

.. _ARM linaro GIT: https://git.linaro.org/landing-teams/working/arm
.. _ARM Gitlab: https://gitlab.arm.com/arm-reference-solutions
.. _Old Change Log: https://git.linaro.org/landing-teams/working/arm/arm-reference-platforms.git/tree/docs/basefvp/change-log.rst
