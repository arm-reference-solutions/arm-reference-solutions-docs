.. _docs/totalcompute/tc2/change-log:

Change Log
==========

.. contents::

This document contains a summary of the new features, changes and
fixes in each release of TC2 software stack.

Version 2022.08.12
------------------

Features added
~~~~~~~~~~~~~~
- Hardware Root of Trust
- Updated Android to AOSP master
- Microdroid based pVM support in Android

*Copyright (c) 2022, Arm Limited. All rights reserved.*
