.. _docs/totalcompute/tc1/change-log:

Change Log
==========

.. contents::

This document contains a summary of the new features, changes and
fixes in each release of TC1 software stack.

Version 2022.10.07
------------------

Features added
~~~~~~~~~~~~~~
- Added support for MTE3/EPAN
- Added support for Firmware Update
- Enabled VHE support in Hafnium to support S-EL0 partitions

Changes
~~~~~~~
- Updated Android to AOSP master
- Updated Trusted Firmware-A and Hafnium to v2.7
- Update Android Common Kernel (ACK) to 5.15
- IP renaming (Klein to Cortex A510, Makalu to Cortex A715, Makalu ELP to Cortex X3)

Version 2022.05.12
------------------

Features added
~~~~~~~~~~~~~~
- Trusty support in Android
- CI700-PMU enabled for profiling
- Added support for ETE and TRBE

Changes
~~~~~~~
- Updated Android to S
- Updated Trusted Firmware-A & Hafnium to v2.6
- Updated SCP firmware to v2.10
- Updated U-Boot to v2022.01
- Updated Trusted Services
- Updated Yocto repositories
- Deprecated android-nano profile

Version 2021.08.17
------------------

Features added
~~~~~~~~~~~~~~
- Memory Tagging Extension (MTE)
- Pointer Authentication Code (PAC)
- Branch Target Identification (BTI)
- Android AOSP to master (May21)
- Android Common Kernel to v5.10
- Trusted Firmware-A & Hafnium to v2.5
- OP-TEE to v3.14.0
- SCP firmware to v2.8
- U-boot to v2021.07
- Yocto to master

*Copyright (c) 2021-2022, Arm Limited. All rights reserved.*
