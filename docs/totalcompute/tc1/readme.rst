.. _docs/totalcompute/tc1/readme:

.. section-numbering::
    :suffix: .

Instructions: Obtaining Total Compute software deliverables
-----------------------------------------------------------
 * To build the TC1 software stack please refer to :ref:`user-guide <docs/totalcompute/tc1/user-guide>`
 * For the list of changes and features added please refer to :ref:`change-log <docs/totalcompute/tc1/change-log>`
 * For further details on the latest release and features please refer to :ref:`release_notes <docs/totalcompute/tc1/release_notes>`

TC Software Stack Overview
--------------------------

The TC1 software consists of firmware, kernel and file system components that can run on the associated FVP.
Following are the Software components:

 #. SCP firmware – System initialization, Clock and Power control
 #. AP firmware – Trusted Firmware-A (TF-A)
 #. Secure Partition Manager
 #. Secure Partitions

    * OP-TEE Trusted OS in Poky
    * Trusted Services in Poky
    * Trusty Trusted OS in Android

 #. U-Boot – loads and verifies the fitImage for poky boot, containing kernel and filesystem or boot Image for Android Verified Boot, containing kernel and ramdisk.
 #. Kernel – supports the following hardware features

    * Mailbox hardware unit
    * PAC/MTE/BTI features

 #. Android

    * Supports PAC/MTE/BTI features

:ref:`Total Compute Platform Software Components <docs/totalcompute/tc1/tc1_sw_stack>`

