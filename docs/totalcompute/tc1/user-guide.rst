.. _docs/totalcompute/tc1/user-guide:

User Guide
==========

.. contents::


Notice
------

The Total Compute 2021 (TC1) software stack uses the `Yocto project <https://www.yoctoproject.org/>`__
to build a Board Support Package (BSP) and a choice of Poky Linux distribution or Android userspace.
The Yocto project uses `Bitbake <https://docs.yoctoproject.org/bitbake/2.0/index.html>`__
to build the software stack.


Prerequisites
-------------

These instructions assume that:
 * Your host PC is running Ubuntu Linux 18.04 LTS.
 * You are running the provided scripts in a ``bash`` shell environment.
 * You are using git version of 2.30.0 or higher.

To resolve the dependencies, run:

::

    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install chrpath gawk texinfo libssl-dev diffstat wget git-core unzip gcc-multilib \
     build-essential socat cpio python python3 python3-pip python3-pexpect xz-utils debianutils \
     iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm git-lfs openssl curl \
     libncurses5 lib32ncurses5-dev libz-dev python-pip u-boot-tools m4 zip gcc-9 libstdc++6 liblz4-tool zstd

To get the latest repo tool from google, run the following commands:

::

    mkdir -p ~/bin
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo
    export PATH=~/bin:$PATH

If syncing and building android, the minimum requirements for the host machine can be found at https://source.android.com/setup/build/requirements, These include:
 * At least 250GB of free disk space to check out the code and an extra 150 GB to build it. If you conduct multiple builds, you need additional space.
 * At least 32 GB of available RAM/swap.
 * Git configured properly using "git config" otherwise it may throw error while fetching the code.

Syncing and building the source code
------------------------------------

There are two distros supported in the TC1 software stack: poky (a minimal distro containing busybox) and android.

To sync code for poky, please follow the steps in "Syncing code" section for BSP only. To sync code for android, please follow the steps for syncing both BSP and Android.

To build the required binaries for poky, please follow the steps in "Board Support Package build" section only. To build the binaries for Android, please follow the steps in both "Board Support Package build" and "Android OS build" sections.

Syncing code
#####################

Create a new folder that will be your workspace, which will henceforth be referred to as ``<tc1_workspace>``
in these instructions.

::

    mkdir <tc1_workspace>
    cd <tc1_workspace>
    export TC1_RELEASE=refs/tags/TC1-2022.10.07

To sync BSP only without Android, run the repo command.

::

    repo init -u https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-manifest -m tc1.xml -b ${TC1_RELEASE} -g bsp
    repo sync -j$(nproc)

To sync both the BSP and Android, run the repo command.

::

    repo init -u https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-manifest -m tc1.xml -b ${TC1_RELEASE} -g android
    repo sync -j$(nproc)


Board Support Package build
############################

::

    cd <tc1_workspace>/bsp
    export DISTRO="poky"
    export MACHINE="tc1"
    source setup-environment
    bitbake tc-artifacts-image

The initial clean build will be lengthy, given that all host utilities are to be built as well as
the target images. This includes host programs (python, cmake, etc.) and the required toolchain(s).

Once the build is successful, all images will be placed in the ``<tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1``
directory.

Note that the BSP includes the Poky Linux distribution, which offers BusyBox-like utilities.


Android OS build
#################

* tc1_swr  : This supports Android display with swiftshader (software rendering).

The android images can be built with or without authentication enabled using Android Verified Boot(AVB).
AVB build is done in userdebug mode and takes a longer time to boot as the images are verified.

The ``build-scripts/tc1/build_android.sh`` script in ``<tc1_workspace>/android`` will patch and build android. This can be passed 2 parameters, ``-d`` for deciding which profile to build and ``-a`` for enabling AVB. The following command shows the help menu for the script:

::

    cd <tc1_workspace>/android
    ./build-scripts/tc1/build_android.sh  -h
    Incorrect script use, call script as:
    <path to build_android.sh> [OPTIONS]
    OPTIONS:
    -d, --distro                            distro version, values supported [android-swr]
    -a, --avb                               [OPTIONAL] avb boot, values supported [true, false], DEFAULT: false

The ``--avb`` option does not influence the way the system boots rather it adds an optional sanity check on the prerequisite images.

As an example, to build Android with software rendering and AVB disabled, run the command:

::

  ./build-scripts/tc1/build_android.sh -d android-swr

To build Android with software rendering and AVB enabled, run the command:

::

  ./build-scripts/tc1/build_android.sh -d android-swr -a true

Android based stack takes considerable time to build, so start the build and go grab a cup of coffee!

Provided components
-------------------

Within the Yocto project, each component included in the TC1 software stack is specified as
a `Bitbake recipe <https://docs.yoctoproject.org/bitbake/2.0/bitbake-user-manual/bitbake-user-manual-intro.html#recipes>`__.
The TC1 recipes are located at ``<tc1_workspace>/bsp/layers/meta-arm/``.

Yocto allows modifying the fetched source code of each recipe component in the
workspace, by applying patches. This is however not a convenient approach for
developers, since creating patches and updating recipes is time-consuming.
To make that easier, Yocto provides the `devtool <https://wiki.yoctoproject.org/wiki/TipsAndTricks/Patching_the_source_for_a_recipe>`__ utility. Devtool creates a
new workspace, in which you can edit the fetched source code and bake images
with the modifications

::

    cd <tc1_workspace>/bsp
    MACHINE=tc1
    DISTRO=poky
    . ./conf/setup-environment

    # create a workspace for a given recipe component
    # recipe-component-name can be of:
    # trusted-firmware-a / scp-firmware / u-boot / linux-arm64-ack
    devtool modify <recipe-component-name>

    # This creates a new workspace for recipe-component-name and fetches source code
    # into "<tc1_workspace>/build-poky/workspace/sources/{trusted-firmware-a,scp-firmware,u-boot,linux-arm64-ack}"
    # edit the source code in the newly created workspace
    # build images with changes on workspace
    # recipe-component-name can be of: trusted-firmware-a / scp-firmware / u-boot / linux-arm64-ack
    bitbake <recipe-component-name>

Firmware Components
###################

Trusted Firmware-A
******************

Based on `Trusted Firmware-A <https://trustedfirmware-a.readthedocs.io/en/latest/>`__

+--------+------------------------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/trusted-firmware-a/trusted-firmware-a-tc.inc  |
+--------+------------------------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/bl1-tc.bin                                     |
|        | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/fip_gpt.bin                                    |
+--------+------------------------------------------------------------------------------------------------------------+


System Control Processor (SCP)
******************************

Based on `SCP Firmware <https://github.com/ARM-software/SCP-firmware>`__

+--------+------------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/scp-firmware/scp-firmware-tc.inc  |
+--------+------------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/scp_ramfw.bin                      |
|        | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/scp_romfw.bin                      |
+--------+------------------------------------------------------------------------------------------------+


U-Boot
******

Based on `U-Boot gitlab <https://gitlab.denx.de/u-boot/u-boot>`__

+--------+---------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/u-boot/u-boot_%.bbappend |
+--------+---------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/u-boot.bin                |
+--------+---------------------------------------------------------------------------------------+


Hafnium
*******

Based on `Hafnium <https://www.trustedfirmware.org/projects/hafnium>`__

+--------+--------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/hafnium/hafnium-tc.inc  |
+--------+--------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/hafnium.bin              |
+--------+--------------------------------------------------------------------------------------+


OP-TEE
******

Based on `OP-TEE <https://github.com/OP-TEE/optee_os>`__

+--------+------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-security/optee/optee-os-tc.inc  |
+--------+------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/optee/tee-pager_v2.bin       |
+--------+------------------------------------------------------------------------------------------+


S-EL0 trusted-services
**********************

Based on `Trusted Services <https://www.trustedfirmware.org/projects/trusted-services/>`__

+--------+-----------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-tc/recipes-security/trusted-services/secure-partitions_git.bb |
+--------+-----------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/crypto-sp.bin                     |
|        | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/internal-trusted-storage.bin      |
|        | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/firmware-update.bin               |
+--------+-----------------------------------------------------------------------------------------------+

Linux
*****

The recipe responsible for building a 5.15 version of the Android Common kernel (`ACK <https://android.googlesource.com/kernel/common/>`__).

+--------+-----------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-kernel/linux/linux-arm-platforms.inc |
+--------+-----------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/Image                             |
+--------+-----------------------------------------------------------------------------------------------+

Trusty
******

Based on `Trusty <https://source.android.com/security/trusty>`__

+--------+---------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/meta-tc/recipes-security/trusty/trusty_git.bb  |
+--------+---------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/lk.bin        |
+--------+---------------------------------------------------------------------------+

Distributions
#############

Poky Linux distro
*****************

The layer is based on the `poky <https://www.yoctoproject.org/software-item/poky/>`__ Linux distribution.
The provided distribution is based on BusyBox and built using glibc.

+--------+-------------------------------------------------------------------------------------------------+
| Recipe | <tc1_workspace>/bsp/layers/openembedded-core/meta/recipes-core/images/core-image-minimal.bb     |
+--------+-------------------------------------------------------------------------------------------------+
| Files  | * <tc1_workspace>/bsp/build-poky/tmp-poky/deploy/images/tc1/fitImage-core-image-minimal-tc1-tc1 |
+--------+-------------------------------------------------------------------------------------------------+


Android
*******

Android AOSP master is supported in this release with device profiles suitable for TC1 machine configuration.
Android is built as a separate project and then booted with the BSP built by Yocto.


Run scripts
###########

Within the ``<tc1_workspace>/bsp/run-scripts/`` are several convenience functions for testing the software
stack. Usage descriptions for the various scripts are provided in the following sections.


Obtaining the TC1 FVP
---------------------

The TC1 FVP is available to partners for build and run on Linux host environments.
Please contact Arm to have access (support@arm.com).


Running the software on FVP
---------------------------

A Fixed Virtual Platform (FVP) of the TC1 platform must be available to run the included run scripts.

The run-scripts structure is as follows:

::

    run-scripts
    |--tc1
       |--run_model.sh
       |-- ...

Ensure that all dependencies are met by running the FVP: ``./path/to/FVP_TC1``. You should see
the FVP launch, presenting a graphical interface showing information about the current state of the FVP.

The ``run_model.sh`` script in ``<tc1_workspace>/bsp/run-scripts/tc1`` will launch the FVP, providing
the previously built images as arguments. Run the ``run_model.sh`` script:

::

       ./run_model.sh
       Incorrect script use, call script as:
       <path_to_run_model.sh> [OPTIONS]
       OPTIONS:
       -m, --model                      path to model
       -d, --distro                     distro version, values supported [poky, android-swr]
       -a, --avb                        [OPTIONAL] avb boot, values supported [true, false], DEFAULT: false
       -t, --tap-interface              [OPTIONAL] enable TAP interface
       -e, --extra-model-params	        [OPTIONAL] extra model parameters

Running Poky
############

::

        ./run-scripts/tc1/run_model.sh -m <model binary path> -d poky

Running Android
###############

::

       If using an android distro, export ANDROID_PRODUCT_OUT variable to point to android out directory
       for eg. ANDROID_PRODUCT_OUT=<tc1_workspace>/android/out/target/product/tc_swr

       For running android with AVB disabled:
        ./run-scripts/tc1/run_model.sh -m <model binary path> -d android-swr

       For running android with AVB enabled:
        ./run-scripts/tc1/run_model.sh -m <model binary path> -d android-swr -a true

When the script is run, two terminal instances will be launched. terminal_s0 used for the SCP,
TF-A, OP-TEE core logs and terminal_s1 used by TF-A early boot, Hafnium, U-boot and Linux.

Once the FVP is running, the SCP will be the first to boot, bringing the AP out of reset.
The AP will start booting from its ROM and then proceed to boot Trusted Firmware-A, Hafnium,
Secure Partitions (OP-TEE, Trusted Services in Poky and Trusty in Android) then U-Boot, and then Linux and Poky/Android.

When booting Poky the model will boot Linux and present a login prompt. Login
using the username ``root``. You may need to hit Enter for the prompt to appear.

The OP-TEE and Trusted Services are initialized in Poky distribution. The functionality of OP-TEE and
core set of trusted services such as Crypto and Internal Trusted Storage can be invoked only on Poky distribution.
For OP-TEE, the TEE sanity test suite can be run using command ``xtest``.
For Trusted Services, run command ``ts-service-test -sg ItsServiceTests -sg CryptoKeyDerivationServicePackedcTests
-sg CryptoMacServicePackedcTests -sg CryptoCipherServicePackedcTests -sg CryptoHashServicePackedcTests
-sg CryptoServicePackedcTests -sg CryptoServiceProtobufTests -sg CryptoServiceLimitTests -sg  DiscoveryServiceTests -v``
for Service API level tests and run command ``ts-demo`` for the demonstration client application.

On Android distribution, Trusty provides a Trusted Execution Environment (TEE).
The functionality of Trusty IPC can be tested using command ``tipc-test -t ta2ta-ipc`` with root privilege.

Firmware Update
---------------

Creating Capsule
################

Firmware Update in the total compute platform uses the capsule update mechanism. Hence, the Firmware Image Package (FIP) binary
has to be converted to a capsule. This can be done with ``GenerateCapsule`` which is present in ``BaseTools/BinWrappers/PosixLike``
of the `edk2 project <https://github.com/tianocore/edk2>`__.

::

       GenerateCapsule -e -o efi_capsule --fw-version 1 --lsv 0 --guid 0d5c011f-0776-5b38-8e81-36fbdf6743e2 --verbose --update-image-index 0 --verbose fip-tc.bin

| "fip-tc.bin" is the input fip file that has the firmware binaries of the total compute platform
| "efi_capsule" is the name of capsule to be generated
| "0d5c011f-0776-5b38-8e81-36fbdf6743e2" is the image type UUID for the FIP image

Loading Capsule
###############

The capsule generated using the above steps has to be loaded into memory during the execution of the model by providing the below FVP arguments.

::

       --data board.dram=<location of capsule>/efi_capsule@0x2000000

This loads the capsule to be updated at address 0x82000000

Updating Firmware
#################

During the normal boot of the platform, stop at the U-Boot prompt and execute the below commands.

::

       TOTAL_COMPUTE# efidebug capsule update -v 0x82000000

This will update the firmware. After it is completed, reboot the platform using the reset command.

::

       TOTAL_COMPUTE# reset

--------------

*Copyright (c) 2021-2022, Arm Limited. All rights reserved.*
