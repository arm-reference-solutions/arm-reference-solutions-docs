.. _docs/totalcompute/tc0/change-log:

Change Log
==========

.. contents::

This document contains a summary of the new features, changes and
fixes in each release of TC0 software stack.

Version 2022.02.25
------------------

Features added
~~~~~~~~~~~~~~
- Maximum Power Mitigation Mechanism (MPMM) Support in SCP. This is an experimental and untested feature.

Changes
~~~~~~~
- Updated Android to S
- Updated Trusted Firmware-A & Hafnium to v2.6
- Updated SCP firmware to v2.9
- Updated U-Boot to v2021.10
- Updated Trusted Services
- Updated Yocto repositories
- Deprecated android-nano profile

Version 2021.07.31
------------------

Features added
~~~~~~~~~~~~~~
- Memory Tagging Extension (MTE)
- Pointer Authentication Code (PAC)
- Branch Target Identification (BTI)

Changes
~~~~~~~
- Updated Android AOSP to master (May21)
- Updated Android Common Kernel to v5.10
- Updated Trusted Firmware-A & Hafnium to v2.5
- Updated OP-TEE to v3.14.0
- Updated SCP firmware to v2.8
- Updated U-boot to v2021.07
- Updated Yocto to master

Version 2021.04.23
------------------

Features added
~~~~~~~~~~~~~~
- Crypto and Storage Trusted Services running at S-EL0.
- Shim Layer at S-EL1 running on top of S-EL2 SPMC (Hafnium).

Changes
~~~~~~~
- Updated Android to R/11.
- DEBUGFS support for Trusted Services added to Android Common Kernel.

Version 2021.02.09
------------------

Features added
~~~~~~~~~~~~~~
- Trusted OS OP-TEE as S-EL1 Secure Partition managed by SEL2 SPMC (Hafnium).
- OP-TEE support in Poky Distribution.
- Arm FF-A driver and FF-A transport support for OP-TEE driver in Android Common Kernel. This is an experimental feature.
- DynamIQ Shared Unit (DSU) with 8 cores. 4 Matterhorn (big) cores + 4 Klein (LITTLE) cores configuration.

Changes
~~~~~~~
- Updated Trusted Firmware-A to version 2.4.
- Updated Hafnium (SPM) to version 2.4.
- Updated U-boot to 2020.10
- Updated SCP firmware to 2.7
- Updated Yocto to 3.2 (Gatesgarth release)

Version 2020.10.29
------------------

Features added
~~~~~~~~~~~~~~
- Power management features: cpufreq and cpuidle.
- SCMI (System Control and Management Interface) support.
- Verified u-boot for authenticating fit image (containing kernel + ramdisk) during poky boot.
- Android Verified Boot (AVB) for authenticating boot and system image during android boot.
- Software rendering on android with DRM Hardware Composer offloading composition to Mali D71 DPU.
- Initial support for Hafnium as Secure Partition Manager (SPM) at S-EL2, managing cactus S-EL1 test Secure Partitions (SPs). The git SHA used for Hafnium is 764fd2eb and for Trusted Firmware-A is 00ad74c7.

Changes
~~~~~~~
- Changed from Android Common Kernel 4.19 to 5.4.
- Changed from Cortex-M7 to Cortex-M3 based System Control Processor.

Version 2020.08.14
------------------

Features added
~~~~~~~~~~~~~~
- Poky Distribution support.
- Android Q/10 Support.
- Android Common Kernel 4.19.
- Trusted Firmware-A.
- Support secure boot based on TBBR specification https://developer.arm.com/documentation/den0006/latest
- System Control Processor firmware.
- Yocto based build system.
- U-Boot bootloader.

--------------

*Copyright (c) 2020-2022, Arm Limited. All rights reserved.*
