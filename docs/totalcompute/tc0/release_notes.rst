.. _docs/totalcompute/tc0/release_notes:

Release notes - 2022.02.25
==========================

.. contents::

Release tag
-----------
The manifest tag for this release is TC0-2022.02.25

Components
----------
The following is a summary of the key software features of the release:
 - Yocto based BSP build supporting Android and Poky distro.
 - Trusted firmware-A for secure boot.
 - System control processor(SCP) firmware for programming the interconnect, doing power control etc.
 - U-Boot bootloader.
 - Hafnium for S-EL2 Secure Partition Manager core.
 - OP-TEE for Trusted Execution Environment (TEE).
 - Trusted Services (Crypto and Internal Trusted Storage).

Hardware Features
-----------------
 - Booker CI with Memory Tagging Unit(MTU) support driver in SCP firmware.
 - GIC Clayton Initialization in Trusted Firmware-A.
 - Mali-D71 DPU and virtual encoder support for display in Linux.
 - MHUv2 Driver for SCP and AP communication.
 - UARTs, Timers, Flash, PIK, Clock drivers.
 - PL180 MMC.
 - DynamIQ Shared Unit (DSU) with 8 cores. 4 Matterhorn (big) cores + 4 Klein (LITTLE) cores configuration.

Software Features
-----------------
 - Poky Distribution support.
 - Android S Support.
 - Android Common Kernel 5.10 with PAC/BTI/MTE
 - With Android S support, the KVM default mode of operation is set to ``protected``. This is a nVHE based mode with kernel running at EL1.
 - Trusted Firmware-A & Hafnium v2.6
 - OP-TEE 3.14.0
 - Support secure boot based on TBBR specification https://developer.arm.com/documentation/den0006/latest
 - System Control Processor (SCP) firmware v2.9
 - Build system based on Yocto master
 - U-Boot bootloader v2021.10
 - Power management features: cpufreq and cpuidle.
 - SCMI (System Control and Management Interface) support.
 - Verified u-boot for authenticating fit image (containing kernel + ramdisk) during poky boot.
 - Android Verified Boot (AVB) for authenticating boot and system image during Android boot.
 - Software rendering on Android with DRM Hardware Composer offloading composition to Mali D71 DPU.
 - Hafnium as Secure Partition Manager (SPM) at S-EL2.
 - OP-TEE as Secure Partition at S-EL1, managed by S-EL2 SPMC (Hafnium)
 - Arm FF-A driver and FF-A Transport support for OP-TEE driver in Android Common Kernel.
 - OP-TEE Support in Poky distribution. This includes OP-TEE client and OP-TEE test suite.
 - Trusted Services (Crypto and Internal Trusted Storage) running at S-EL0.
 - Trusted Services test suite added to poky distribution.
 - Shim Layer at S-EL1 running on top of S-EL2 SPMC (Hafnium) used by Trusted Services running in S-EL0.

Platform Support
----------------
 - This Software release is tested on TC0 Fast Model platform (FVP).
   - Supported Fast model version for this release is 11.17.18

Known issues or Limitations
---------------------------
1. At the U-Boot prompt press enter and type "boot" to continue booting else wait
   until it boots automatically. The wait duration depends upon the time
   difference in CPU frequency and FVP operating frequency.

Support
-------
For support email:  support-arch@arm.com

--------------

*Copyright (c) 2020-2022, Arm Limited. All rights reserved.*
