.. _docs/totalcompute/readme:

Total Compute Platform
----------------------

Total Compute is an approach to moving beyond optimizing individual IP to take a system-level solution view of the SoC that puts use cases and experiences at the heart of the designs.

Total Compute focuses on optimizing Performance, Security, and Developer Access across Arm’s IP, software, and tools. This means higher-performing, more immersive, and more secure experiences on devices coupled with an easier app and software development process.

TC2 release tags
================

`TC2-2022.08.12 <https://arm-reference-solutions-docs.readthedocs.io/en/tc2-2022.08.12/docs/totalcompute/tc2/readme.html>`_

TC1 release tags
================

`TC1-2022.05.12 <https://arm-reference-solutions-docs.readthedocs.io/en/tc1-2022.05.12/tc1/readme.html>`_

`TC1-2021.08.17 <https://arm-reference-solutions-docs.readthedocs.io/en/tc1-2021.08.17/docs/totalcompute/tc1/readme.html>`_

TC0 release tags
================

`TC0-2022.02.25 <https://arm-reference-solutions-docs.readthedocs.io/en/tc0-2022.02.25/docs/totalcompute/tc0/readme.html>`_

`TC0-2021.07.31 <https://arm-reference-solutions-docs.readthedocs.io/en/tc0-2021.07.31/docs/totalcompute/readme.html>`_

`TC0-2021.04.23 <https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/TC0-2021.04.23/docs/totalcompute/tc0>`_

`TC0-2021.02.09 <https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/TC0-2021.02.09/docs/totalcompute/tc0>`_

