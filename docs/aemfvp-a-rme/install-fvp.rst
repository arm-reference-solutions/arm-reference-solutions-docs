.. contents::

Obtain the Armv-A Base RevC AEM FVP
-----------------------------------
Running at speeds comparable to the real hardware, Fixed Virtual Platforms 
(FVPs) are complete simulations of an Arm system, including processor, 
memory and peripherals.

Arm Architecture FVPs
^^^^^^^^^^^^^^^^^^^^^
Architecture Envelope Models (AEM) FVPs are fixed configuration virtual 
platforms of Armv8-A, Armv9-A and Armv8-R architectures. The platform includes 
the Architecture Envelope Model (AEM) for Armv8 and Armv9 (consolidated into 
Armv-A) and a comprehensive set of SystemIP. Details of the AEM FVP platform 
can be found in the Fast Models Fixed Virtual Platforms Reference Guide. The 
Compliance FVP is used for testing architecture compliance testing and is not 
suitable for software development. 

To Learn more about ARM FVP models check out documentation at `FVP-doc`_

This release has been tested and validated with the FVP's listed below and are
therefore the recommended versions to use depending on the host architecture.

+-------------------+----------------------------------+
| Host Architecture | Armv-A Base RevC AEM FVP Version |
+===================+==================================+
|     x86_64        | `11.24.11-x86_64`_                |
+-------------------+----------------------------------+
|     aarch64       | `11.24.11_aarch64`_               |
+-------------------+----------------------------------+

Installing FVP
^^^^^^^^^^^^^^
Information and download links for Architecture Envelop Model (AEM) FVP's can
be found at `Arm Ecosystem FVPs`_ page.

- Navigate to “Arm Architecture FVPs” section.
- Click on “Neoverse Fremont Reference Design FVP” link to obtain the list of 
  available Neoverse RD-Fremont Reference Design FVPs.
- Select a FVP build under the section “Download RD-Fremont” based on the host 
  machine architecture.
- For AArch64 host, click “Armv-A Base RevC AEM FVP (AArch64 Linux)” link.
- For x86-64 host, click “Armv-A Base RevC AEM FVP (x86 Linux)” link.


Download and extract the tar file to any directory of choice (<FVP-DIR>)
(Recommended dir : rme-stack/FVP). After extracting the FVP bins with 
``tar -xvzf``, the model's executable binary can be found as

For x86 arch:

``<FVP-DIR>/Base_RevC_AEMvA_pkg/models/Linux64_GCC-9.3/FVP_Base_RevC-2xAEMvA``.

For aarch64 arch:

``<FVP-DIR>/Base_RevC_AEMvA_pkg/models/Linux64_armv8l_GCC-9.3/FVP_Base_RevC-2xAEMvA``.

Documentation for the downloaded FVP can be found under
``<FVP-DIR>/Base_RevC_AEMvA_pkg/doc``.

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _FVP-doc: https://developer.arm.com/documentation/100966/1124/?lang=en
.. _Arm Ecosystem FVPs: https://developer.arm.com/downloads/-/arm-ecosystem-models
.. _11.24.11-x86_64: https://developer.arm.com/-/media/Files/downloads/ecosystem-models/FVP_Base_RevC-2xAEMvA_11.24_11_Linux64.tgz
.. _11.24.11_aarch64:  https://developer.arm.com/-/media/Files/downloads/ecosystem-models/FVP_Base_RevC-2xAEMvA_11.24_11_Linux64_armv8l.tgz
