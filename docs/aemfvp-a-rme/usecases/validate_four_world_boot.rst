.. contents::

Validate four World Boot
========================

This use case demonstrates that the stack is able to do a four world boot (Root,
Secure, Realm and Non-Secure) described in Arm CCA software.
Test boots the software stack and fvp, and parses the log files to validate 
that four worlds were successfully booted.

**Note:** This use case can be automated.

Pre-requisites
--------------
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.

Running Use case
----------------

::

    ./model-scripts/aemfvp-a-rme/boot.sh -p aemfvp-a-rme validate

Expected outcome
----------------
After the test has been completed, user should see a pass result and log 
message indicating "Four world boot successful".
This test proves all 4 expected Arm CCA worlds i.e Secure, Realm, Normal and 
root world are running on FVP model. 

::

    Secure world boot successful
    Realm world boot successful
    Non-Secure world boot successful
    Four world boot successful

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*
