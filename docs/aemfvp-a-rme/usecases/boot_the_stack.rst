.. contents::

Boot the Stack
==============

Introduction
-----------
This use case allows the use of buildroot as the filesystem and boot the 
software stack on the fast model. FVP will be booted with binaries from 
rme-stack/output/aemfvp-a-rme/ using bootloader, firmware image package (fip), 
kernel and buildroot root file system.

**Note:** This use case can be automated.

Pre-requisites
--------------
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.

Running Use case
----------------

To launch the FVP and boot the stack in an interactive shell to explore, use the
following command. This will provide a login to buildroot, use ``root`` for both
username and password.

::

    ./model-scripts/aemfvp-a-rme/boot.sh -p aemfvp-a-rme shell


**Note:** If Xterm is not installed on the host machine, please use ``telnet``
to connect to the terminals to see the output. The port number to connect to
are displayed in the console debug prints as below

::

 terminal_0: Listening for serial connection on port 5000
 terminal_2: Listening for serial connection on port 5002
 terminal_1: Listening for serial connection on port 5001
 terminal_3: Listening for serial connection on port 5003

For e.g. to connect to terminal_0, execute following in a different shell
::

 telnet localhost 5000

Expected outcome
----------------
After the boot has been completed, user is expected to see the login prompt.

::

    Welcome to Buildroot

    buildroot login:

A successfull boot proves ARM CCA software stack has all components working as 
expected on FVP model.

**Note:** Poweroff the guest gracefully with ``poweroff`` command after use.
