.. contents::

Run Realm Kvm Unit Tests
========================

`KVM-Unit-Tests`_ are set of unit tests for the KVM component. This release
includes `Kvm-Unit-Tests-CCA`_ which has a few test for KVM in realm. 

Set of kvm-unit-test are developed to test various RME architectural
features. Some of these include tests for Realm-RSI (Realm Service Interface),
PSCI support in realm security state, GIC to validate interrupt support in the
Realm, and Realm attestation to check support for remote attestation for realm
payloads.

**Note:** This use case can be automated.

Pre-requisites
--------------
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.

Running Use case
----------------
To launch the KVM-Unit Tests use the command below:

::

    ./model-scripts/aemfvp-a-rme/kvm-unit-tests.sh -p aemfvp-a-rme

Here the kvm-unit-tests are already built and present in /kvm-unit-tests/arm 
dir of booted stack on FVP. kvm-unit-tests.sh runs following commands to 
trigger test cases.

::

    cd /kvm-unit-tests/arm;
    ./run-realm-tests

Expected outcome
----------------
This will launch a number of KVM unit tests that are run in Realm security
state.
After the tests have been run, log will indicate the status of individual 
kvm test case as follows

::

  RUNNING REALM KVM-UNIT-TESTS 
  lkvm run -k ./selftest.flat -m 16 -c 2 --name guest-179
  PASS: selftest: setup: smp: number of CPUs matches expectation
  INFO: selftest: setup: smp: found 2 CPUs
  PASS: selftest: setup: mem: memory size matches expectation
  INFO: selftest: setup: mem: found 16 MB
  SUMMARY: 2 tests

    # KVM session ended normally.
  lkvm run -k ./selftest.flat -m 16 -c 1 --name guest-192
  PASS: selftest: vectors-kernel: und
  PASS: selftest: vectors-kernel: svc
  SKIP: selftest: vectors-kernel: pabt test not supported in a realm
  SUMMARY: 3 tests, 1 skipped

    # KVM session ended normally.
  lkvm run -k ./selftest.flat -m 16 -c 1 --name guest-204
  PASS: selftest: vectors-user: und
  PASS: selftest: vectors-user: svc
  SUMMARY: 2 tests

This completes the Realm kvm-unit-tests run.

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _KVM-Unit-Tests: http://www.linux-kvm.org/page/KVM-unit-tests
.. _Kvm-Unit-Tests-CCA: https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca