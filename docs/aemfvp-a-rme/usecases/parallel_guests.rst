.. contents::

Create Realm and normal VM parallely
------------------------------------
kvmtool is a virtualization utility to launch linux guest images. lkvm also 
supports creation of realm guests that confirms with ARM RME specs. This 
virtual machine will be coming instantiated in realm security state.

During the build step of this stack, buildroot packages a kernel and file system
so that realm virtual guests can use them. These are found in the path
``/realm`` of the ``host-fs.ext4``.

Host supports screen command which allows you to launch multiple background 
terminals used to bring realm and normal vm.

Pre-requisites
^^^^^^^^^^^
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.
- Launch FVP for interactive shell in accordance with section "Boot the Stack".
- Login to buildroot prompt.

Running Use case
^^^^^^^^^^^^^^^^

First, boot the stack and login to buildroot.

- Then use kvmtool to launch a realm guest in a screen.

  ::

    screen -md -S "realm"  lkvm run --realm -c 2 -m 256 -k /realm/Image -d /realm/realm-fs.ext4 -p earlycon

- verify that the screen session is running

  ::

    # screen -ls
    There are screens on:
      198.realm       (Detached)

- Launch a normal VM in a screen.

  ::

    screen -md -S "nsvm"  lkvm run -c 2 -m 256 -k /realm/Image -d /realm/realm-fs.ext4 -p earlycon

- verify that the screen session is running

  ::

    # screen -ls
    There are screens on:
      198.realm       (Detached)
      214.nsvm        (Detached)

- User can connect and disconnect from these screen terminals using 
  ``screen -r`` to connect and ``CTRL-A + d`` to disconnect and route 
  back to original terminal.

  ::

    Connect to realm VM
    screen -r 198.realm


where 
::
  --realm,               launch guest in realm security state
  -c, --cpus <n>         Number of CPUs
  -m, --mem <size>.      Virtual machine memory size
  -d, --disk <image>     Disk  image or rootfs directory
  -k, --kernel <kernel>  Kernel to boot in virtual machine

Expected outcome
^^^^^^^^^^^^^^^^

This will start launching two lkvm guest and takes some time for it to boot up. 
After the boot has been completed, user is expected to see the login prompt.
Connect to both screens and login with username ``root`` and password ``root``

::
  
  Welcome to Buildroot
  buildroot login:

top command on host indicates both vms are running
::

  PID  PPID USER     STAT   VSZ %VSZ %CPU COMMAND
  188   187 root     S     462m  12%  25% lkvm run --realm -c 2 -m 256 -k /realm
  204   203 root     S     462m  12%   0% lkvm run -c 2 -m 256 -k /realm/Image -

This usecase proves RME stack supports running realm and normal vms parallely.

**Note:** Poweroff the guest gracefully with ``poweroff`` command after use.