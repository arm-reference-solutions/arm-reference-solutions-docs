.. contents::

Check SSH to linux guest in realm
---------------------------------
This use case ensures networking is enabled between the host machine and lkvm 
realm vm. After the lkvm guest has been booted up, we check ping and ensure 
ssh to guest is successfull and network to a realm machine is up.

build-scripts copies dropbear ssh keys on both host and guest vm to enable ssh 
between them. Test sets up a bridge at host and during realm creation an 
interface is added to this bridge.

Host creates a bridge device vbr0 with IP address 192.168.33.1 and starts a 
DHCP server so that guests vm can request an IP address in ``192.168.33.XX`` 
range. Realm VM creation script includes ``--network mode=tap,script=`` where 
script is used for adding interface to host's bridge. After the realm boot, 
host establishes ssh connection with IP assigned to realm.

**Note:** This use case can be automated.

Pre-requisites
^^^^^^^^^^^
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.

Running Use case
^^^^^^^^^^^^^^^^
To launch the ssh connection test use the command below:

::

    ./model-scripts/aemfvp-a-rme/ssh_connection.sh -p aemfvp-a-rme

we have packed a ssh_conn_test.sh script in host vm's rootfs. This script 
launches a realm vm and establishes ssh connection to it.

Expected outcome
^^^^^^^^^^^^^^^^
Ping and ssh connection are succesfull from host to realm vm.

::

    Welcome to Buildroot
    buildroot login: lkvm guest boot complete
    PING 192.168.33.190 (192.168.33.190): 56 data bytes
    --- 192.168.33.190 ping statistics ---
    3 packets transmitted, 3 packets received, 0% packet loss
    ssh:
    Host '192.168.33.190' key accepted unconditionally.
    SSH from Host to Guest Successful
    Stop lkvm guest
    Info: KVM session ended normally.

Successful execution of this usecase proves network connection can be 
established between host and a realm VM.