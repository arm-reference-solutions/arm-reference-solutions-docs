.. contents::

Run Trusted Firmware-A Tests
============================

`TrustedFrimware-A-Tests`_ (TF-A-Tests) is a suite of baremetal tests to
exercise the Trusted Firmware-A (TF-A) features. It enables strong TF-A
functional testing without dependency on a Rich OS. It provides a basis for TF-A
developers to validate their own platform ports and add their own test cases. In
this release, standard tests from TrustedFrimware-A-Tests have been built in.
Refer the `Trusted Firmware-A Tests Documentation`_ for more details.

Currently we are running standard TFTF test suite. In accordance with TF 
documentation tftf has been build with ENABLE_REALM_PAYLOAD_TESTS=1 which 
builds and packages  tftf for Realm Management Extension (RME) testing.

**Note:** This use case can be automated.

Pre-requisites
--------------
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.

Running Use case
----------------

Tests can be launched by executing following command.

::

    ./model-scripts/aemfvp-a-rme/tftf.sh -p aemfvp-a-rme

Expected outcome
----------------
tftf.sh launches entire standard test suite and after tests have been run it's 
expected to generate test result as such.

::

  TEST COMPLETE                                                 Passed
  ******************************* Summary *******************************
  > Test suite 'Framework Validation'
                                                                  Passed
  ...
  ...
  > Test suite 'RMI and SPM tests'
                                                                  Passed
  > Test suite 'Realm payload at EL1'
                                                                  Passed
  =================================
  Tests Skipped : 78
  Tests Passed  : 155
  Tests Failed  : 0
  Tests Crashed : 0
  Total tests   : 233
  =================================
  NOTICE:  Exiting tests.

This completes the tftf tests run.


-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _TrustedFrimware-A-Tests: https://trustedfirmware-a-tests.readthedocs.io/en/latest/index.html#
.. _Trusted Firmware-A Tests Documentation: https://trustedfirmware-a-tests.readthedocs.io/en/latest/#trusted-firmware-a-tests-documentation
