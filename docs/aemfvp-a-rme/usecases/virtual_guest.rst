.. contents::

Create a Virtual Guest in Realm World
=====================================
kvmtool is a virtualization utility to launch linux guest images. lkvm also 
supports creation of realm guests that confirms with ARM RME specs. This 
virtual machine will be coming instantiated in realm security state.

During the build step of this stack, buildroot packages a kernel and file system
so that realm virtual guests can use them. These are found in the path
``/realm`` of the ``host-fs.ext4``.

Pre-requisites
--------------
- Arm Base AEM software stack has been downloaded and build.
- FVP Model is downloaded and FVP's path has been exported as FVP_DIR.
- Move to rme-stack path.
- Launch FVP for interactive shell in accordance with section "Boot the Stack".
- Login to buildroot prompt.

Running Use case
----------------

First, boot the stack and login to buildroot. Then use kvmtool to launch a realm
guest.

::

 lkvm run --realm -c 2 -m 256 -k /realm/Image -d /realm/realm-fs.ext4 -p earlycon

where 
::
  --realm,               launch guest in realm security state
  -c, --cpus <n>         Number of CPUs
  -m, --mem <size>.      Virtual machine memory size
  -d, --disk <image>     Disk  image or rootfs directory
  -k, --kernel <kernel>  Kernel to boot in virtual machine

Expected outcome
----------------

This will start launching a realm guest and takes some time for it to boot up. 
After the boot has been completed, user is expected to see the login prompt.

::
  
  Welcome to Buildroot
  buildroot login:

**Note:** Poweroff the guest gracefully with ``poweroff`` command after use.

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*