.. contents::

Build the Software Stack
------------------------

The stack build is driven by a manifest file, which is a collection of all
component repositories present in this reference stack. Two files
``pinned-<platform_name>.xml`` and ``<platform_name>.xml`` are provided with the
release. Pinned manifest points to stack that is tagged, components are known to
work together, to deliver a working reference software release. The non-pinned
manifest file tracks components mainline development, where the user gets access
to the latest software available but could also mean that at times the combined
stack may not work, as components tend to move out of step following their
individual development schedules.

The repository hosting both these manifest files is 
`Arm Base AEM Manifest`_, and the manifest files are named:

- pinned-aemfvp-a-rme.xml
- aemfvp-a-rme.xml

The table below lists all components in this reference stack and links to the
repository, and the revision validated for release
(i.e. used in pinned-aemfvp-a-rme.xml).

+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Component                | Repository Url                                               | Revision                                 |
+==========================+==============================================================+==========================================+
| Linux-CCA                | https://gitlab.arm.com/linux-arm/linux-cca                   | cca-full/rmm-v1.0-eac5                   |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Kvmtool-CCA              | https://gitlab.arm.com/linux-arm/kvmtool-cca                 | cca/rmm-v1.0-eac5                        |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Kvm-Unit-Tests-CCA       | https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca          | cca/rmm-v1.0-eac5                        |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Build-scripts            | https://gitlab.arm.com/arm-reference-solutions/build-scripts | refs/tags/AEMFVP-A-RME-2023.12.22        |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Model-scripts            | https://gitlab.arm.com/arm-reference-solutions/model-scripts | refs/tags/AEMFVP-A-RME-2023.12.22        |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Trusted-Firmware-A       | https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git  | d0574da589330f676af5a9a3ed5a08a82993e666 |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Trusted-Firmware-A-Tests | https://git.trustedfirmware.org/TF-A/tf-a-tests.git          | 11886e848b790408ba69d636d9b52cb7204f94f9 |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Hafnium                  | https://git.trustedfirmware.org/hafnium/hafnium.git          | ed7dee80b329dcddf3e34e2e5e6bda2476ed596d |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| TF-RMM                   | https://git.trustedfirmware.org/TF-RMM/tf-rmm.git            | f83164d964fec97b98b74975bb8f7a4a67f828bf |
+--------------------------+--------------------------------------------------------------+------------------------------------------+
| Buildroot                | https://github.com/buildroot/buildroot                       | refs/tags/2023.08                        |
+--------------------------+--------------------------------------------------------------+------------------------------------------+

Getting Started
^^^^^^^^^^^^^^^

A summary of the steps followed is listed below, with the commands needed to
execute listed in each sub-section.

- Create an empty folder to store the software source files, this will be
  mounted inside the container.
- Run the container in interactive mode.
- Download the software source files.
- Build the software stack.

Run Container Image
^^^^^^^^^^^^^^^^^^^

::

    mkdir rme-stack
    ./container.sh -v </absolute/path/to/rme-stack> run

This will execute the container and the mount point inside it is the same
as the host path provided. You are now inside the container and the prompt 
shall look like

::

    USER:HOSTNAME:~/workspace$

This completes the procedure to setup the container-based build environment.

**Note:** When the container starts the user will be in the root directory.

Download Sources
^^^^^^^^^^^^^^^^

As mentioned above, a manifest file and repo tool are used to download the
software stack. The repository that hosts manifest files is
`Arm Base AEM Manifest`_. Change to the mounted directory and
get the sources.

::

    cd </absolute/path/to/rme-stack>
    repo init -u https://git.gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-manifest.git -m pinned-aemfvp-a-rme.xml -b refs/tags/AEMFVP-A-RME-2023.12.22
    repo sync -c -j $(nproc) --fetch-submodules --force-sync --no-clone-bundle

In order to reduce the size of the commit history that is downloaded (and 
reduce the time taken to download the platform software stack), append 
``–depth=1`` to the repo init command.

Buildroot Build
^^^^^^^^^^^^^^^

Buildroot is a simple, efficient and easy-to-use tool to generate a complete 
Linux systems through cross-compilation. In order to achieve this, buildroot 
is able to generate a cross-compilation toolchain, a root filesystem, a Linux 
kernel image and a bootloader for a target and can be used for any 
combination, independently, one can for example use an existing cross- 
compilation toolchain, and build only the root filesystem with buildroot.

Buildroot boot on baseaem fvp allows the use of buildroot as the filesystem 
and boot the software stack on the fast model. This document describes the 
procedure to build and execute the software stack with buildroot as the root 
filesystem.

Build Stack
^^^^^^^^^^^


::

    ./build-scripts/aemfvp-a-rme/build-test-buildroot.sh -h

The command above displays a help message detailing all supported flags and
commands that can be used with this script. To clean (any previous build
artefacts), build and package the stack, execute:

::

    ./build-scripts/aemfvp-a-rme/build-test-buildroot.sh -p aemfvp-a-rme all

After the above step completes, the necessary binaries would be built and
located in ``rme-stack/output/aemfvp-a-rme/`` directory. This completes the
building of the reference stack.

Contents of output should have required bins and images
::

    ls rme/output/aemfvp-a-rme/
    bl1.bin  fip.bin  fip-std-tests.bin  host-fs.ext4  Image

To exit the container type ``exit``.

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _Arm Base AEM Manifest: https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-manifest
