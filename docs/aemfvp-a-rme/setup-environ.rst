.. contents::

Setup Build Environment
-----------------------

The instructions provided here were verified on Ubuntu 22.04 LTS and on both
x86_4 and aarch64 host environments. All the instructions were executed on a
``bash`` shell. The build environment is container-based where a user builds a
container image from a container file on which all build dependencies are
satisfied and isolated from the host machine.

Pre-requisites
^^^^^^^^^^^

- x86_64 or aarch64 host environment
- Minimum free disk space of 30 GB;
- Version control utility ``git``;
- To execute the FVP binary ``telnet`` and ``xterm``;

::

    sudo apt update && sudo apt install git telnet xterm net-tools sshpass

Git tool setup
^^^^^^^^^^^^^^^^^^^^^^^
In order to simplify downloading the software stack, repo tool can be used. 
This section explains the procedure to setup git and repo tool.
Install Git by using the following command

::

    sudo apt install git

Configure git user name and email address

::

    git config --global user.name "<your-name>"
    git config --global user.email "<your-email@example.com>"

Install Container Engine
^^^^^^^^^^^^^^^^^^^^^^^^

In this release, "Docker" has been used as the container engine. Please refer to
`Docker official instructions`_ as there are several methods available, ensuring
to install the following `docker-engine`_ and *buildx-plugin*. After installation
is complete, refer to the `post-installation steps`_ on how to manage docker as
non-root user.

Installation steps can be referenced from `Install Docker Engine on Ubuntu`_.
Few instructions extracted from the above links to get a basic Docker engine
installed are as below:

::

    #Step 1: Setup docker repository
    sudo apt-get update && sudo apt-get install ca-certificates curl gnupg
    sudo install -m 0755 -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    
    #Step 2: Install the engine (latest version)
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

    #Step 3: Execute docker without typing sudo every time.
    sudo groupadd docker
    sudo usermod -aG docker $USER

    #After, log out and log in for the change to take effect; if on a VM, restart it.
    
    # Step 4: Confirm the docker and user has been set correctly.
    sudo docker run hello-world


Obtain the Container Repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The container file, wrapper and utility scripts are located in
`this repository`_.

::

  git clone --branch AEMFVP-A-RME-2023.12.22 https://git.gitlab.arm.com/arm-reference-solutions/docker.git

The wrapper script *container.sh* defines the container file and image name by
default, and this can be changed either with flags *-f* and *-i* or by editing
the file itself.

To see all options available, change to the directory where the repository
mentioned above is cloned and execute:

::

  ./container.sh -h

Build Container Image
^^^^^^^^^^^^^^^^^^^^^

To build the container image, execute:

::

  ./container.sh build

This script supports building x86 and aarch64 docker container images, by 
default it reads the host architecture and selected appropriate docker arch.

To confirm the image was successfully built, and assuming the default image
name was used, executing:

::

    docker image list

shall output a similar message as below:

::

    REPOSITORY        TAG       IMAGE ID       CREATED       SIZE
    aemfvp-builder    latest    2fa7ce18f57a   7 mins ago    1.83GB

-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _Docker official instructions: https://docs.docker.com/engine/install/
.. _post-installation steps: https://docs.docker.com/engine/install/linux-postinstall/
.. _this repository: https://gitlab.arm.com/arm-reference-solutions/docker
.. _docker-engine: https://docs.docker.com/engine/
.. _Install Docker Engine on Ubuntu: https://docs.docker.com/engine/install/ubuntu/