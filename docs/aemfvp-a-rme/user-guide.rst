*******************************************************
Reference Arm CCA integration stack Software User Guide
*******************************************************

.. contents::
|
Introduction
------------

This is a reference integration stack for Arm's Confidential Compute 
Architecture (CCA) software's implementation. It demonstrates support for 
Arm's Realm Management Extension (RME) architecture feature implemented in a 
series of open source components.

Below is an overview of the software stack and its components:

.. image:: docs/aemfvp-a-rme/images/sw-components.png
  :width: 1130
  :height: 480
  :alt: SW components diagram


The software stack for this platform includes an integration of `Linux-CCA`_,
`Kvmtool-CCA`_, `Trusted-Firmware-A`,  `Hafnium`_, `TF-RMM`_ and other
componnets.

In this document, instructions to build and run a firmware and Linux stack on
Arm's Base AEM (Architecture Envelope Model) FVP platform model is provided.

|
Arm Confidential Compute
------------------------

Confidential compute is a broad term for technologies that reduce the need to
trust a computing infrastructure, such as the need for processes to trust
operating system (OS) kernel and the need for virtual machines to trust
hypervisors.

Confidential compute performs computation within a trustworthy hardware-backed
secure environment. This shields code and data from observation or modification
by privileged software and hardware agents. Any application or operating system
executing in a confidential computing environment can expect to execute in
isolation from any non-trusted agent in the rest of the system.

More details on Arm CCA can be found at our official documentation here at 
`Arm Confidential Compute Architecture`_

The following diagram shows the software components in an Arm CCA system.

.. image:: docs/aemfvp-a-rme/images/CCA-Software-components.png
  :width: 867
  :height: 416
  :alt: CCA Software components

|
Arm Realm Management Extension (RME)
------------------------------------

RME supports a new type of attestable isolation environment called a Realm.
Realms allow lower-privileged software, such as application or a Virtual
Machine to protect its content and execution from attacks by higher-privileged
software, such as an OS or a hypervisor. Higher-privileged software retains the
responsibility for allocating and managing the resources that are utilized by a
Realm, but cannot access its contents, nor affect its execution flow. The
hypervisor however, manages system resources.

The diagram below gives the security model for Arm CCA. For more details refer
to the `Arm CCA security model`_.

.. image:: docs/aemfvp-a-rme/images/RME-security-model.png
  :width: 660
  :height: 416
  :alt: RME security model

RME architecture
^^^^^^^^^^^^^^^^

Realm Management Extension or RME is an extension to Arm v9 arcitecture that
defines the set of hardware features and properties that are required to comply
with the Arm CCA architecture. As shown in above diagram the RME extends the 
Arm security model to four states, those are -

- Secure state
- Non-secure state
- Realm state
- Root state

SCR_EL3.[NSE,NS] controls PE security state.

+------------------+----------------+
| SCR_EL3.[NSE,NS] | Security state |
+==================+================+
|     0b00         |   Secure       |
+------------------+----------------+
|     0b01         |   Non-secure   |
+------------------+----------------+
|     0b10         |   NA           |
+------------------+----------------+
|     0b11         |   Realm        |
+------------------+----------------+

There is no encoding for root state. While in Exception level 3, the current
security state is always root, regardless of the value of SCR_EL3.{NSE,NS}.

RME provides hardware-based isolation. In that,

- Execution in the Realm security state cannot be observed or modified by an
  agent associated with either the Non-secure or the Secure security state.
- Execution in the Secure security state cannot be observed or modified by an
  agent associated with either the Non-secure or the Realm security state.
- Execution in the Root security state cannot be observed or modified by an
  agent associated with any other security state.
- Memory assigned to the Realm security state cannot be read or modified by an
  agent associated with either the Non-secure or the Secure security state.
- Memory assigned to the Secure security state cannot be read or modified by
  an agent associated with either the Non-secure or the Realm security state.
- Memory assigned to the Root security state cannot be read or modified by an
  agent associated with any other security state.

Realm World
^^^^^^^^^^^
The Realm world provides an execution environment for VMs that is isolated 
from the Normal and Secure worlds. VMs require control from the Host in the 
Normal world. To allow for full control of Realm creation and execution, the 
Arm CCA system provides:

- Realm Management Extension, which are the hardware extensions that are 
  required by the architecture to allow isolated Realm VM execution.
- Realm Management Monitor, which is part of the firmware that is required to 
  manage Realm creation and execution under requests from a Normal world Host.

The Realm Management Monitor (RMM) executes at EL2 in Realm security state 
(R-EL2). Its responsibilities are to:

- Provide an execution environment for Realms, which run at R-EL1 / R-EL0.
- Isolate Realms from one another.

|
Running the Reference Stack
--------------------------
AEM-A FVP stack has been tested to run on ubuntu 22.04 and components are built
in a specific environment on custom docker. Ensure to execute following three
steps in order : 

- Setup a linux machine with `Setup Build Environment`_ which sets up a docker 
  image to build stack.

- Reference stack can be downloaded and build inside the container using 
  `Build the Software Stack`_ guide. At the end of the steps mentioned in 
  guide, all necessary binary files would have been built.

- CCA Stack executables are run on ARM FVP model which can be downloaded 
  with `Obtain AEM FVP`_ guide.

Host Execution
^^^^^^^^^^^^^^
If the choice is to execute on the host environment, change the directory to the
location where the stack was downloaded and built before proceeding to the next
section which details the usecases that user can try out on the reference stack.

::

  cd </absolute/path/to/the/rme-stack>

**Export Model path**

when executing usecases for 'Host Execution' environment 
model-script looks for environment variable to pick model executables.
Set ``FVP_DIR`` variable before launching the model.

::

    export FVP_DIR=<FVP-DIR>

    (Recommended dir : rme-stack/FVP)


Container Execution
^^^^^^^^^^^^^^^^^^^
After downloading and building the reference stack, the reference stack can be
executed. The software stack can be executed either in a container environment
or on the host environment (assuming appropriate FVP is downloaded and
extracted). If the choice is to execute in the container environment, the
command is to be executed.

::

  ./container.sh -v </absolute/path/to/rme-stack> run

**NOTE** : Ensure to set ``Export Automate``


**Export Model Path in Docker**

User can skip  `Obtain AEM FVP`_ step altogether and just run the use cases 
inside the container ``aemfvp-builder`` where stack was built. container build 
already installs a compatible ARM AEM FVP in a path which can be set for 
use cases to pick executable from. 

Run following to setup FVP_DIR correctly inside docker container based use 
case execution.

::
  
    export FVP_DIR=/home/<user name>/models


Export Automate
^^^^^^^^^^^^^^^
FVP Model starts four terminals, the output of these terminals are captured in 
``uart<num>.log`` in ``logs/aemfvp-a-rme`` path. These four terminals are
launched and used for logging from following entities : 


- terminal_0 : MCP logs.
- terminal_1 : SCP logs.
- terminal_2 : Secure partition logs.
- terminal_3 : RMM logs.

User can set environment variable to enable automated runs, this allows the 
usecases to be run in a docker or remote enviroment where the xterm terminals 
launched by FVP model are not visible/accessible to user.

Setting Automation mode

::

    export Automate=true


|
Supported use cases
-------------------

This software stack uses buildroot as the root filesystem and the user of this 
guide has the following options to experiment/explore Arm's CCA features:

- `Boot the stack`_ : Boot the stack for interactive use.
- `Validate four world boot`_ : Boot the stack and validate that the four 
  worlds of the Confidential Compute Architecture booted successfully.
- `Virtual Guest`_ : Boot the stack for interactive use and create virtual 
  guests running in the realm world.
- `TF-A Tests`_ : Run Standard Test Suite from the TrustedFirmware Tests.
- `KVM Unit Tests`_ : Run KVM-Unit-Tests in a virtual guest running in the 
  realm world.
- `Parallel Guest`_ : Parallely create realm and normal guest vms.
- `Network Test`_ : Check networking is enabled between host and guest realm vm.

**Note:** Booting the FVP with the stack is supported on the host machine and 
in the docker container ``aemfvp-builder``.

|
Release
-------
This release of AEM-A-RME FVP software stack comes with following main updates.

- CCA spec has been updated to EAC5 release for TF-RMM with supporting linux 
  and kvm repos.
- Build supported on Ubuntu Jammy *22.04*.
- GCC version for build updated to *12.3.rel1*.
- Buildroot version update to *2023.08*.
- Linux kernel *v6.7.0*.
- Scalable Vector extension enabled in FVP runs.
- ARM public FVP version used *11.24_11*.


Release
^^^^^^^
Refer release document `AEMFVP-A-RME-2023.12.22-REL`_ for more information 
about software stack tags/branches, FVP version used and summary of testing.

**Release TAG** : AEMFVP-A-RME-2023.12.22

**FVP Used** : 11.24.11


Previous Release Tags
^^^^^^^^^^^^^^^^^^^^^
Table below lists the release tags for this platform software stack and
the corresponding FVP version that is recommended to be used along
with the listed release tag. The summary of the changes introduced and
tests validated in each release is listed in the release note, the link to
which is in the 'Release Tag' column in the table below.

+-------------------------------+-------------------------+----------------------------------+
|         Release Tag           |       FVP Version       |           Release Doc            |
+===============================+=========================+==================================+
|  `AEMFVP-A-RME-2023.12.22`_   |       11.24.11          |  `AEMFVP-A-RME-2023.12.22-REL`_  |
+-------------------------------+-------------------------+----------------------------------+
|  `AEMFVP-A-RME-2023.09.29`_   |       11.23.9           |  `AEMFVP-A-RME-2023.09.29-REL`_  |
+-------------------------------+-------------------------+----------------------------------+
|  `AEMFVP-A-RME-2023.06.30`_   |       11.22.14          |  `AEMFVP-A-RME-2023.06.30-REL`_  |
+-------------------------------+-------------------------+----------------------------------+
|  `AEMFVP-A-RME-2023.03.17`_   |       11.20.15          |  `AEMFVP-A-RME-2023.03.17-REL`_  |
+-------------------------------+-------------------------+----------------------------------+


-------------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*

.. _Linux-CCA: https://gitlab.arm.com/linux-arm/linux-cca
.. _Kvmtool-CCA: https://gitlab.arm.com/linux-arm/kvmtool-cca
.. _Trusted-Firmware-A: https://trustedfirmware-a.readthedocs.io/en/latest/
.. _Hafnium: https://review.trustedfirmware.org/plugins/gitiles/hafnium/hafnium/+/HEAD/README.md
.. _TF-RMM: https://tf-rmm.readthedocs.io/en/latest/
.. _AEMFVP-A-RME-2023.12.22: https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/AEMFVP-A-RME-2023.12.22?ref_type=tags
.. _AEMFVP-A-RME-2023.09.29: https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/AEMFVP-A-RME-2023.09.29?ref_type=tags
.. _AEMFVP-A-RME-2023.06.30: https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/AEMFVP-A-RME-2023.06.30?ref_type=tags
.. _AEMFVP-A-RME-2023.03.17: https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/AEMFVP-A-RME-2023.03.17?ref_type=tags
.. _Arm Confidential Compute Architecture: https://www.arm.com/architecture/security-features/arm-confidential-compute-architecture
.. _Arm CCA security model: https://developer.arm.com/documentation/DEN0096/latest/
.. _AEMFVP-A-RME-2023.12.22-REL: docs/aemfvp-a-rme/releases/AEMFVP-A-RME-2023.12.22/release_note.rst
.. _AEMFVP-A-RME-2023.09.29-REL: docs/aemfvp-a-rme/releases/AEMFVP-A-RME-2023.09.29/release_notes.rst
.. _AEMFVP-A-RME-2023.06.30-REL: docs/aemfvp-a-rme/releases/AEMFVP-A-RME-2023.06.30/release_notes.rst
.. _AEMFVP-A-RME-2023.03.17-REL: docs/aemfvp-a-rme/releases/AEMFVP-A-RME-2023.03.17/release_note.rst
.. _Boot the stack: docs/aemfvp-a-rme/usecases/boot_the_stack.rst
.. _Validate four world boot: docs/aemfvp-a-rme/usecases/validate_four_world_boot.rst
.. _TF-A Tests: docs/aemfvp-a-rme/usecases/tfa_tests.rst
.. _KVM Unit Tests: docs/aemfvp-a-rme/usecases/kvm_ut_tests.rst
.. _Virtual Guest: docs/aemfvp-a-rme/usecases/virtual_guest.rst
.. _Parallel Guest: docs/aemfvp-a-rme/usecases/parallel_guests.rst
.. _Network Test: docs/aemfvp-a-rme/usecases/network_test.rst
.. _Setup Build Environment: docs/aemfvp-a-rme/setup-environ.rst
.. _Build the Software Stack: docs/aemfvp-a-rme/build-stack.rst
.. _Obtain AEM FVP: docs/aemfvp-a-rme/install-fvp.rst
