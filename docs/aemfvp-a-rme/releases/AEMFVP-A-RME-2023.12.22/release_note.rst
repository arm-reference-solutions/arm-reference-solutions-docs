AEMFVP-A-RME-2023.12.22
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's reference stack for CCA software 
  architecture.
- Arm's CCA has RMM, TF-A and Linux repos now EAC5 compliant for RME.


Test Coverage
-------------

The following tests have been completed using 11.24.11 version of the FVP on
both x86_64 and aarch64 host machines.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm
- Booting realm VM in realm security state.
- SSH connection Test to realm VM.
- Parallel boot of realm and normal world VM.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca
  - Branch   : cca-full/rmm-v1.0-eac5

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca
  - Branch   : cca/rmm-v1.0-eac5

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : d0574da589330f676af5a9a3ed5a08a82993e666

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : ed7dee80b329dcddf3e34e2e5e6bda2476ed596d

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : f83164d964fec97b98b74975bb8f7a4a67f828bf

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Branch   : 2023.08

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca
  - Branch   : cca/rmm-v1.0-eac5

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : 11886e848b790408ba69d636d9b52cb7204f94f9

--------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*
