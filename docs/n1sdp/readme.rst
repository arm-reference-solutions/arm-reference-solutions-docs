.. _docs/n1sdp/readme:

***********************
Arm Reference Platforms
***********************

.. contents::

Introduction
============

Arm produces open source software stacks for a variety of platforms, serving
as a reference to help enable product development based on Arm IP for a range
of target markets and applications.

We use these software stacks to demonstrate:

- Integration of multiple open source software components, including firmware,
  operating systems, applications, and services.

- Upstream support for configuring the latest Arm IP.

- Software features including secure services and power management.

- Alignment with Arm specifications and open source community initiatives.

Neoverse N1 Software Development Platform
=========================================

Neoverse N1 SDP is an infrastructure segment development platform available
for licensing to Arm partners. The platform supports a reference open-source
software stack based on Trusted Firmware-A, SCP-firmware, EDK II UEFI, GRUB,
Linux, and user-space components. Supporting code is available either directly
in the relevant upstream projects, or VIA public-facing git repositories.

The software release supports Linux stable kernel booting Ubuntu server
distribution or an optional minimal BusyBox filesystem.

Please follow the `user guide`_ to sync, build, and run the software stack on
the N1SDP.

More details are available in the following
`Neoverse N1 SDP getting started guide`_.

For questions and discussions, visit `our Arm community forums`_
For technical support, please contact us at support@arm.com

.. _user guide: docs/n1sdp/user-guide.rst
.. _our Arm community forums: https://community.arm.com/developer/tools-software/oss-platforms/f/dev-platforms-forum
.. _Neoverse N1 SDP getting started guide: https://community.arm.com/oss-platforms/w/docs/457/n1sdp-getting-started-guide

----------

*Copyright (c) 2021, Arm Limited. All rights reserved.*
