**********
Change Log
**********

.. contents::

This document contains a summary of the incremental features, changes, fixes
and known issues in each release of the N1SDP stack. It is listed in the order
of latest to oldest.

Tagged Version - N1SDP-2024.06.14
=================================

``This is the final stable release for N1SDP and there are no plans to release further updates for N1SDP.
Anyone using N1SDP will be expected to update, maintain and bug fix for themselves going forward from
July 2024.``

Features and Fixes
------------------
- Rebased to the stable Linux kernel version ``6.6.10``.
- Rebasing the firmware components to the following:

  - SCP release tag ``v2.14.0``.
  - TF-A release tag ``v2.11.0``.
  - edk2 release tag ``edk2-stable202405``.
  - edk2-Platforms commit id ``35bca3ca71c004b7f3d93c6f33724796c6b1bf0b``.
- Upstreamed the following features in edk2/edk2-platform repositories:

  - Add support for NT_FW_CONFIG.
  - Migrate the DBG2 serial port to IOFPGA uart and related changes in edk2.
  - Add support for UEFI persistent variables storage.
- Added a halt for SCP boot in case of DDR training failure.
- Added the following optimizations to build-scripts:

  - Enabled use of busybox provided init.
  - Building perf as part of the Ubuntu Image.
  - Added a cleanup function to handle unexpected failures during build of the Linux Kernel.

Precautions
-----------
- The system thermal monitoring and control features are not yet calibrated,
  therefore do not operate the unit above room temperature (approximately 25°C).
- The N1SDP is intended for use within a laboratory or engineering development
  environment. Do not use the N1SDP near equipment that is sensitive to
  electromagnetic emissions, for example, medical equipment.
- Never subject the board to high electrostatic potentials.
  Observe Electrostatic Discharge (ESD) precautions when handling any board.

  - Always wear a grounding strap when handling the board.
  - Avoid touching the component pins or any other metallic elements.
- Update/Change board firmware only if MCC FW asks to do so,
  refer to the `potential damage`_ page for more information
- Kindly note, the USB 3.0 ports and the audio jacks available on the front
  panel of the N1SDP case are NOT connected and are not usable. Ports have
  been covered by the **arm** label on the ATX case.

Known Issues and Limitations
----------------------------
- Patches providing proof of concept support for Xilinx CCIX accelerator
  endpoints are no longer included in this release.
- PCIe root port is limited to GEN3 speed due to the on-board PCIe
  switch itself only supporting up to GEN3 speed.
- Page Request Interface (PRI) feature is not available in both SMMUs
  interfacing with the PCIe root ports.
- Currently only Micron 8GB single Rank DIMMs
  (part number: MTA9ASF1G72PZ-2G6D1) and 16GB dual Rank DIMMs
  (part number:MTA18ASF2G72PDZ-2G6E1) are supported.
- CPU soft lockup messages have been observed occasionally during long
  stress tests on the C2C system.
- On-board HDMI connection is not supported for graphics output. A PCIe
  graphics card can be used for graphics support.
- If either of the two boards needs to boot up in a single chip mode
  with a C2C setup, then the other board should be powered off.
- Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.

Support
-------
- For support email: support@arm.com
- For reporting security vulnerabilities, please refer `Vulnerability reporting`_
  page.

Tagged Version - N1SDP-2023.06.22
=================================
Features and Fixes
------------------
- UEFI persistent variables storage.
- Updated the Ubuntu installer to Ubuntu 20.04.5 .
- Docker container to be used as the build environment.
- Added fix for the broken SCMI communication between MCP and SCP.
- Added fix for SCP debug build boot issue.
- PCC refresh which adds fix for spurious reboots when system shutdown is
  requested from SCP or AP.
- Rebase to the latest stable Linux kernel version ``6.3.3``.
- Migrate to the stable EDK2 version ``edk2-stable202305``.
- Migrate to the latest mainline version of System Control Processor (SCP)
  firmware, edk2-platforms and Trusted Firmware-A components.
- MCC refresh to v122 to include:
  - Added qspi erase commands to selftest menu

Precautions
-----------
- The system thermal monitoring and control features are not yet calibrated,
  therefore do not operate the unit above room temperature (approximately 25°C).
- The N1SDP is intended for use within a laboratory or engineering development
  environment. Do not use the N1SDP near equipment that is sensitive to
  electromagnetic emissions, for example, medical equipment.
- Never subject the board to high electrostatic potentials.
  Observe Electrostatic Discharge (ESD) precautions when handling any board.

  - Always wear a grounding strap when handling the board.
  - Avoid touching the component pins or any other metallic elements.
- Update/Change board firmware only if MCC FW asks to do so,
  refer to the `potential damage`_ page for more information
- Kindly note, the USB 3.0 ports and the audio jacks available on the front
  panel of the N1SDP case are NOT connected and are not usable. Ports have
  been covered by the **arm** label on the ATX case.

Known Issues and Limitations
----------------------------
- Patches providing proof of concept support for Xilinx CCIX accelerator
  endpoints are no longer included in this release.
- PCIe root port is limited to GEN3 speed due to the on-board PCIe
  switch itself only supporting up to GEN3 speed.
- Page Request Interface (PRI) feature is not available in both SMMUs
  interfacing with the PCIe root ports.
- Currently only Micron 8GB single Rank DIMMs
  (part number: MTA9ASF1G72PZ-2G6D1) and 16GB dual Rank DIMMs
  (part number:MTA18ASF2G72PDZ-2G6E1) are supported.
- CPU soft lockup messages have been observed occasionally during long
  stress tests on the C2C system.
- On-board HDMI connection is not supported for graphics output. A PCIe
  graphics card can be used for graphics support.
- If either of the two boards needs to boot up in a single chip mode
  with a C2C setup, then the other board should be powered off.
- Multiple UEFI shell entries in UEFI Boot Manager Menu after enabling UEFI
  persistent variables storage.

Support
-------
- For support email: support@arm.com
- For reporting security vulnerabilities, please refer `Vulnerability reporting`_
  page.

Tagged Version - N1SDP-2022.06.22
=================================
Features and Fixes
------------------
- Add support for Trusted Board Boot and non-trusted firmware config.
- N1SDP sensor library moved out of the SCP codebase and is now
  included as part of the board firmware.
- Rebase to the latest stable Linux kernel version ``5.18.4``.
- Migrate the build environment to Ubuntu Linux 20.04 LTS.
- Migrate to the stable EDK2 version ``edk2-stable202202``.
- Migrate to the latest mainline version of System Control Processor (SCP)
  firmware and Trusted Firmware-A components.
- Add support for CMake in build-scripts to build SCP firmware.
- MCC Refresh to v121 to include:

  - Add a defensive check against preloading image with size exceeding 64MB
    in DDR3 memory.

Precautions
-----------
- The system thermal monitoring and control features are not yet calibrated,
  therefore do not operate the unit above room temperature (approximately 25°C).
- The N1SDP is intended for use within a laboratory or engineering development
  environment. Do not use the N1SDP near equipment that is sensitive to
  electromagnetic emissions, for example, medical equipment.
- Never subject the board to high electrostatic potentials.
  Observe Electrostatic Discharge (ESD) precautions when handling any board.

  - Always wear a grounding strap when handling the board.
  - Avoid touching the component pins or any other metallic elements.
- Update/Change board firmware only if MCC FW asks to do so,
  refer to the `potential damage`_ page for more information
- Kindly note, the USB 3.0 ports and the audio jacks available on the front
  panel of the N1SDP case are NOT connected and are not usable. Ports have
  been covered by the **arm** label on the ATX case.

Known Issues and Limitations
----------------------------
- Patches providing proof of concept support for Xilinx CCIX accelerator
  endpoints are no longer included in this release.
- PCIe root port is limited to GEN3 speed due to the on-board PCIe
  switch itself only supporting up to GEN3 speed.
- Page Request Interface (PRI) feature is not available in both SMMUs
  interfacing with the PCIe root ports.
- Currently only Micron 8GB single Rank DIMMs
  (part number: MTA9ASF1G72PZ-2G6D1) and 16GB dual Rank DIMMs
  (part number:MTA18ASF2G72PDZ-2G6E1) are supported.
- CPU soft lockup messages have been observed occasionally during long
  stress tests on the C2C system.
- On-board HDMI connection is not supported for graphics output. A PCIe
  graphics card can be used for graphics support.
- If either of the two boards needs to boot up in a single chip mode
  with a C2C setup, then the other board should be powered off.
- Occasional spurious reboots have been observed when system shutdown is
  requested from SCP or AP.

Support
-------
- For support email: support@arm.com
- For reporting security vulnerabilities, please refer `Vulnerability reporting`_
  page.

Tagged Version - N1SDP-2021.10.12
=================================
Features and Fixes
------------------
- Migration of source code repository from `ARM linaro GIT`_ to `ARM Gitlab`_.
- Migration of build system from Yocto to BASH based scripting environment.
- Minimal BusyBox based root filesystem support.
- Rebase to the latest stable Linux kernel version ``5.10.61``.
- Migrate to the latest stable EDK2 version ``edk2-stable202108``.
- Update Grub to latest release tag grub-2.06.
- Migrate to the latest master version of System Control Processor (SCP)
  firmware and Trusted Firmware-A components.
- MCC Refresh to v117 to include:

  - QSPI programming speed improvements.
  - Case fans can be controlled through FAN_SPEED IOFPGA register.

Precautions
-----------
- The system thermal monitoring and control features are not yet calibrated,
  therefore do not operate the unit above room temperature (approximately 25°C).
- The N1SDP is intended for use within a laboratory or engineering development
  environment. Do not use the N1SDP near equipment that is sensitive to
  electromagnetic emissions, for example, medical equipment.
- Never subject the board to high electrostatic potentials.
  Observe Electrostatic Discharge (ESD) precautions when handling any board.

  - Always wear a grounding strap when handling the board.
  - Avoid touching the component pins or any other metallic elements.
- Update/Change board firmware only if MCC FW ask to do so,
  refer to `potential damage`_ page for more information
- Kindly note, the USB 3.0 ports and the audio jacks available on the front
  panel of the N1SDP case are NOT connected and are not usable. They will be
  removed in the later versions.

Known Issues and Limitations
----------------------------
- Patches providing proof of concept support for Xilinx CCIX accelerator
  endpoints are no longer included in this release.
- PCIe root port is limited to GEN3 speed due to the on-board PCIe
  switch itself only supporting up to GEN3 speed.
- Page Request Interface (PRI) feature is not available in both SMMUs
  interfacing with the PCIe root ports.
- Currently only Micron 8GB single Rank DIMMs
  (part number: MTA9ASF1G72PZ-2G6D1) and 16GB dual Rank DIMMs
  (part number:MTA18ASF2G72PDZ-2G6E1) are supported.
- Stability issues have been observed on long stress tests of the
  system.
- On-board HDMI connection is not supported for graphics output. A PCIe
  graphics card can be used for graphics support.
- If either of the two boards needs to boot up in a single chip mode
  with a C2C setup, then the other board should be powered off.
- CCIX port on N1SDP as a PCIe root host is not supported in UEFI EDK2.

Disclaimer
----------
- Limited Testing for now due to the current global scenario, to be revisited once
  we get back on site.

Support
-------
For support email: support-subsystem-enterprise@arm.com
For reporting security vulnerabilities, please refer `Vulnerability reporting`_
page.

Change logs for the previous releases
=====================================
- Refer to the `Old Change Log`_ for detailed information on software features
  and changes for the previous releases.

.. _ARM linaro GIT: https://git.linaro.org/landing-teams/working/arm
.. _ARM Gitlab: https://gitlab.arm.com/arm-reference-solutions
.. _Old Change Log: https://git.linaro.org/landing-teams/working/arm/arm-reference-platforms.git/tree/change-log.rst?h=n1sdp-v2
.. _potential damage: https://community.arm.com/developer/tools-software/oss-platforms/w/docs/604/notice-potential-damage-to-n1sdp-boards-if-using-latest-firmware-release
.. _Vulnerability reporting: docs/n1sdp/vulnerability-reporting.rst

----------

*Copyright (c) 2020-2024, Arm Limited. All rights reserved.*
