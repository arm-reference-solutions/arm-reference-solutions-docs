*********************
Troubleshooting Guide
*********************

Introduction
============

The `user guide`_ documentation typically suffices in most cases
but there could be failures on rare occasions. This page provides
solutions for known issues that could affect the use of the
platform software stack.

Ubuntu build error
==================

A build-time error stating "A branch named 'n1sdp-branch' already exists." might
be encountered during the Ubuntu build if the build gets terminated during the
process. To fix this, remove the "n1sdp-branch" by following the steps below.

::

  cd <n1sdp_workspace>/linux
  git checkout -f <BRANCH>
  git branch -D n1sdp-branch

Supported options for ``<BRANCH>`` are:

* <TAG>
  (This option fetches a particular release tag pointed by the <TAG> field.
  The latest release tag is ``N1SDP-2024.06.14``)
* n1sdp
  (This option fetches the n1sdp branch used for code development and could
  contain patches that are not available in the above branches/tags.)

**Note:** Above mentioned steps will also delete local changes or patches in the
linux component.

Boot hang in UEFI with ASSERT from VariableRuntimeDxe
=====================================================

This issue, albeit occasional, stems from corruption in the UEFI persistent
storage region. Please execute the following commands on the MCC console to
erase the UEFI persistent storage region.

::

    Cmd> debug
    Debug> selftest
    Debug> T
    Debug> 0
    Debug> exit
    Cmd> REBOOT

This will reset the UEFI persistent storage to the default settings by erasing
the SCP QSPI flash and reflashing the SCP RAM and FIP region on the next boot,
which should go through successfully.

.. _user guide:
 user-guide.rst

--------------

*Copyright (c) 2024, Arm Limited. All rights reserved.*
