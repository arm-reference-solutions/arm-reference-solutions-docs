.. _docs/infra/readme:

**As of 3rd April 2023, this page has been superseded by
https://gitlab.arm.com/infra-solutions/reference-design/docs/infra-refdesign-docs**

..
    The history of documentation that resided here can be seen in
    commit f9fbebdb25e69c6da7e395adfc8e8a4b107aba84 and earlier.
